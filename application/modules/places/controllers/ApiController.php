<?php

class Places_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        if (!isset($this->_currentUser)) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody('Unauthorized');
            return;
        }

        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted');

        $status = $this->_getParam('status');
        $types = $this->_getParam('types');
        $countries = $this->_getParam('countries');
        $name = $this->_getParam('name');
        $createBy = $this->_getParam('createBy');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();

        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($deleted) && $filter["deleted"] = $deleted;
        isset($status) && $filter["status"] = $status;
        isset($countries) && $filter["countries"] = explode(',', $countries);
//        isset($types) && $filter["types"] = explode(',', $types);
        isset($name) && $filter["name"] = $name;
        isset($createBy) && $filter["createBy"] = $createBy;

        if (!isset($createBy)) {
//            if ($this->_currentUser->getType() == Users_Model_User::TYPE_NO_ADMIN) {
//                if ($this->_currentUser->getPermission() == Users_Model_User::PERMISSION_READ) {
            $publicTypes = array();
            $privateTypes = array();
            $service = Users_Service_PlaceType::getInstance();

            if (!isset($types)) {
                if ($this->_currentUser->getType() == Users_Model_User::TYPE_NO_ADMIN) {
                    if ($this->_currentUser->getPermission() == Users_Model_User::PERMISSION_READ) {
                        $public_PlaceTypes = $service->getList(array(
                            "userId" => $this->_currentUser->getId(),
                            "access" => Users_Model_PlaceType::ACCESS_PUBLIC
                                ));

                        foreach ($public_PlaceTypes as $type) {
                            $publicTypes[] = $type->getPlaceTypeId();
                        }

                        $private_PlaceTypes = $service->getList(array(
                            "userId" => $this->_currentUser->getId(),
                            "access" => Users_Model_PlaceType::ACCESS_PRIVATE
                                ));

                        foreach ($private_PlaceTypes as $type) {
                            $privateTypes[] = $type->getPlaceTypeId();
                        }
                    }
                }
            } else {
                $types = explode(',', $types);
                foreach ($types as $id) {
                    $type = $service->getByPK($id, $this->_currentUser->getId());
                    if ($type) {
                        if ($type->getAccess() == Users_Model_PlaceType::ACCESS_PUBLIC) {
                            $publicTypes[] = $id;
                        } else {
                            $privateTypes[] = $id;
                        }
                    }else{
                        $filter["types"] = $types;
                    }
                }
            }

            if (count($publicTypes) > 0 && count($privateTypes) > 0) {
                $orFilter = array();
                $orFilter['public'] = $publicTypes;
                $orFilter['private'] = $privateTypes;
                $filter["or"] = $orFilter;
            } else if (count($publicTypes) > 0 && count($privateTypes) == 0) {
                $filter["types"] = $publicTypes;
                $filter['isPrivate'] = 0;
            } else if (count($publicTypes) == 0 && count($privateTypes) > 0) {
                $filter["types"] = $privateTypes;
            }
//                }


            if ($status == Places_Model_Place::STATUS_INPROGRESS && $this->_currentUser->getType() == Users_Model_User::TYPE_NO_ADMIN) {
                $countries = $this->getConfirmAccessCountries();
                if (count($countries) == 0) {
                    $this->getResponse()
                            ->setHttpResponseCode(200)
                            ->appendBody(json_encode("User Can not access to any Places"));
                    return;
                } else {
                    $filter["countries"] = $countries;
                }
            }
//            }
        }



        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $places_service = Places_Service_Place::getInstance();
        $places = $places_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($places as $item) {
            $item = $item->toArray();
            if (isset($item['typeId'])) {
                $placeType = PlaceTypes_Service_PlaceType::getInstance()->getByPk($item['typeId']);
                if ($item['typeId']) {
                    $item["placeType"] = $placeType->toArray();
                };
            }

            if (isset($item["countryId"])) {
                $country = Countries_Service_Country::getInstance()->getByPk($item["countryId"])->toArray();
                $item["region"] = Regions_Service_Region::getInstance()->getByPk($country["regionId"])->toArray();
                $item["area"] = Regions_Service_Area::getInstance()->getByPk($country["areaId"])->toArray();
                $item["country"] = $country;
            }

            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'items' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Places_Service_Place::getInstance();
        $place = $service->getByPK($id);

        if ($place) {
            $item = $place->toArray();
            if (isset($item['typeId'])) {
                $placeType = PlaceTypes_Service_PlaceType::getInstance()->getByPk($item['typeId']);
                if ($item['typeId']) {
                    $item["placeType"] = $placeType->toArray();
                };
            }

            if (isset($item["countryId"])) {
                $country = Countries_Service_Country::getInstance()->getByPk($item["countryId"])->toArray();
                $item["region"] = Regions_Service_Region::getInstance()->getByPk($country["regionId"])->toArray();
                $item["area"] = Regions_Service_Area::getInstance()->getByPk($country["areaId"])->toArray();
                $item["country"] = $country;
            }

            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($item));
        } else {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode("Not Found!"));
        }
    }

    public function postAction()
    {
        $place_service = Places_Service_Place::getInstance();
        $params = $this->getParams();

        if (!isset($this->_currentUser)) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody('Unauthorized');
            return;
        }

        if ($this->_currentUser->getType() != Users_Model_User::TYPE_ADMIN) {
            $country_service = Countries_Service_Country::getInstance();
            $country = $country_service->getByPk($params['countryId']);
            if (!$country) {
                $this->getResponse()
                        ->setHttpResponseCode(404)
                        ->appendBody("Not Found Country");
                return;
            }

            $user_country_service = Users_Service_Country::getInstance();
            $userCountry = $user_country_service->getByPK($params['countryId'], $this->_currentUser->getId());
            if (!$userCountry) {
                $this->getResponse()
                        ->setHttpResponseCode(550)
                        ->appendBody("No Permission");
                return;
            }

            $placeType_service = PlaceTypes_Service_PlaceType::getInstance();
            $placType = $placeType_service->getByPk($params['typeId']);
            if (!$placType) {
                $this->getResponse()
                        ->setHttpResponseCode(401)
                        ->appendBody("In valid Place Type");
                return;
            }
        }

//        if (!count($_FILES['file'])) {
//            $this->getResponse()
//                    ->setHttpResponseCode(400)
//                    ->appendBody('Photo in Required!');
//            return;
//        }


        if (!isset($params["id"])) {
            $place = new Places_Model_Place();
            $place->fill($params);
            $place->setCreateBy($this->_currentUser->getId());
            $place->setNew(true);
        } else {
            $place = $place_service->getByPK($params['id']);
            if (!$place instanceof Places_Model_Place) {
                $this->getResponse()
                        ->setHttpResponseCode(404)
                        ->appendBody('Not Found (Place)');
                return;
            }
            $place->fill($params);
            $place->setNew(false);
        }


        if (isset($_FILES['file'])) {
            $place->setPhoto($this->saveImage($_FILES['file']));
        }

        $result = $place_service->save($place);

        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
        }
    }

    public function putAction()
    {
        $service = Places_Service_Place::getInstance();
        $params = $this->getParams();

        $place = $service->getByPK($params['id']);
        //TO Do
        if (!$place instanceof Places_Model_Place) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Place)');
            return;
        }

        $place->fill($params);
        $place->setUpdateDate('now');
        $place->setNew(false);
        $place = $service->save($place);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($place->toArray()));
    }

    public function deleteAction()
    {
        $service = Places_Service_Place::getInstance();
        $placeId = $this->_getParam('id');
        $place = $service->getByPK($placeId);

        if (!$place instanceof Places_Model_Place) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Place)");
            return;
        }

        $service->remove($place);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode("Success"));
    }

    protected function saveImage($file)
    {
        $basePath = realpath(APPLICATION_PATH . '/../public');

        if (!file_exists($basePath . '/upfiles/places/')) {
            @mkdir($basePath . '/upfiles/places/', 0777, true);
        }

        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $path = $basePath . '/upfiles/places/';
        $path .= '/' . $timestamp . '_' . $file['name'];

        move_uploaded_file($file['tmp_name'], $path);

        $name = rawurlencode($file['name']);

        $locatin = '/upfiles/places/' . $timestamp . '_' . $name;

        return $locatin;
    }

    protected function getConfirmAccessCountries()
    {
        $service = Users_Service_Country::getInstance();
        $countries = $service->getList(array(
            "userId" => $this->_currentUser->getId(),
            "write" => Users_Model_Country::PERMISSION_FULL
                ));

        $list = array();
        if ($countries) {
            foreach ($countries as $country) {
                $list[] = $country->getCountryId();
            }
        }

        return $list;
    }

}


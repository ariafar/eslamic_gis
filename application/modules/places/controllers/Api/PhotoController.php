<?php

class Places_Api_PhotoController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function postAction()
    {
        if (!isset($this->_currentUser)) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody('Unauthorized');
            return;
        }

        $placeId = $this->_getParam('id');
        $placeService = Places_Service_Place::getInstance();
        $place = $placeService->getByPk($placeId);

        if (!$place) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody('Not Found Place');
            return;
        }

        $basePath = realpath(APPLICATION_PATH . '/../public');

        if (!file_exists($basePath . '/upfiles/palces/')) {
            @mkdir($basePath . '/upfiles/places/', 0777, true);
        }

        $date = new DateTime();
        $timestamp = $date->getTimestamp();

        $path = $basePath . '/upfiles/places/';
        $path .= '/' . $timestamp . '_' . $_FILES['file']['name'];


        move_uploaded_file($_FILES['file']['tmp_name'], $path);

        $name = rawurlencode($_FILES['file']['name']);
        $locatin = '/upfiles/places/' . '/' . $timestamp . '_' . $name;
        $place->setPhoto($locatin);
        $place->setNew(false);
        $place = $placeService->save($place);

        $item = $place->toArray();
        if (isset($item["countryId"])) {
            $country = Countries_Service_Country::getInstance()->getByPk($item["countryId"])->toArray();
            $item["region"] = Regions_Service_Region::getInstance()->getByPk($country["regionId"])->toArray();
            $item["area"] = Regions_Service_Area::getInstance()->getByPk($country["areaId"])->toArray();
            $item["country"] = $country;
        }


        $this->getResponse()->clearBody();
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($item));
    }

}

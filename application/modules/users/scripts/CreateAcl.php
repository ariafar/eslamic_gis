<?php

error_reporting(E_ALL);
ini_set('display_errors', true);

class Users_Script_CreateAcl
{

    private static $resources = array();

    public function execute()
    {
        /**
         * Build Resources list
         */
        $mdlDir   = opendir(APPLICATION_PATH . '/modules');
        while (($file = readdir($mdlDir)) !== false) {
            if (!is_dir(APPLICATION_PATH . '/modules/' . $file)) {
                continue;
            }
            if ($file == '..' || $file == '.') {
                continue;
            }

            $module = $file;
            $cntDir = APPLICATION_PATH . '/modules/' . $module . '/controllers';
            self::_fetchControllers(ucfirst($module), null, $cntDir);
        }

        closedir($mdlDir);

        $roleSrv = Users_Service_Role::getInstance();
        $permSrv = Users_Service_RolePermission::getInstance();
        $filter  = array(
            'context'    => 'users',
            'context_id' => 'Admin'
        );
        $roles = $roleSrv->getList($filter, null, 0, $c);
        if (count($roles) > 0) {
            $adminRole = current($roles);
        } else {
            $adminRole = new Users_Model_Role();
            $adminRole->setId(1);
            $adminRole->setTitle('admin');
            $adminRole->setContext('users');
            $adminRole->setContextId('Admin');
            $roleSrv->save($adminRole);
        }

        $permSrv->removeRolePermissions($adminRole->getId());

        foreach (self::$resources as $res) {
            $perm = new Users_Model_RolePermission();
            $perm->setContext('system');
            $perm->setAction($res);
            $perm->setItemId('*');
            $perm->setRoleId($adminRole->getId());
            $perm->setPermission(Users_Model_RolePermission::PERMISSION_ALLOWED);
            $permSrv->save($perm);
        }

        $filter    = array(
            'context'    => 'users',
            'context_id' => 'Guest'
        );

        $roles = $roleSrv->getList($filter, null, 0, $c);
        if (count($roles) > 0) {
            $guestRole = current($roles);
        } else {
            $guestRole = new Users_Model_Role();
            $guestRole->setTitle('Guest');
            $guestRole->setContext('users');
            $guestRole->setContextId('Guest');
            $roleSrv->save($guestRole);
        }

        $permSrv->removeRolePermissions($guestRole->getId());
        $guestResources = array(
            'default:index:index',
            'default:error:error',
            'default:error:index',
            'users:index:index',
            'users:auth:login',
            'users:auth:logout',
            'users:auth:denied',
            'users:auth:facebook',
            'users:auth:twitter',
            'users:auth:twittercallback',
            'users:auth:google',
            'users:auth:googleaccept',
            'users:auth:yahoo',
            'users:auth:verifyinvite',
            'users:api:post',
            'users:api:get',
            'sites:index:index',
        );
        foreach ($guestResources as $res) {
            $perm = new Users_Model_RolePermission();
            $perm->setContext('system');
            $perm->setAction($res);
            $perm->setItemId('*');
            $perm->setRoleId($guestRole->getId());
            $perm->setPermission(Users_Model_RolePermission::PERMISSION_ALLOWED);
            $permSrv->save($perm);
        }

        $urSrv = Users_Service_UserRole::getInstance();
        $uRole = $urSrv->getByPK(0, $guestRole->getId());
        if (!$uRole instanceof Users_Model_UserRole) {
            $uRole = new Users_Model_UserRole();
            $uRole->setUserId(0);
            $uRole->setRoleId($guestRole->getId());
            $urSrv->save($uRole);
        }
    }

    private function _fetchControllers($module, $parent, $directory)
    {
        if (!is_dir($directory)) {
            return;
        }

        $cntHnd = opendir($directory);

        while (($file = readdir($cntHnd)) !== false) {

            if (is_file($directory . '/' . $file)) {
                if (!preg_match('/Controller/', $file)) {
                    continue;
                }

                $controller = substr($file, 0, -14);
                if ($parent !== null) {
                    $class = $module . '_' . $parent . '_' . $controller . 'Controller';
                } else {
                    $class = $module . '_' . $controller . 'Controller';
                }
                include_once ($directory. '/' . $file);

                $reflector  = new ReflectionClass($class);
                $methods    = $reflector->getMethods();
                $controller = preg_replace('/([A-Z])/', '-$1', lcfirst($controller));

                foreach ($methods as $method) {
                    if (!preg_match('/Action$/', $method->name)) {
                        continue;
                    }

                    $action      = substr($method->name, 0, -6);
                    if ($parent != null) {
                        $resource = $module . ':' . $parent . '_' . $controller . ':' . $action;
                    } else {
                        $resource = $module . ':' . $controller . ':' . $action;
                    }

                    self::$resources[] = strtolower($resource);
                }

            } else {
                if (   ($file != '.' && $file != '..')
                    && is_dir($directory. '/' . $file)
                ) {
                    if ($parent != null) {
                        self::_fetchControllers($module, $parent . '_' . $file, $directory . '/' . $file);
                    } else {
                        self::_fetchControllers($module, $file, $directory . '/' . $file);
                    }
                }
            }
        }

        closedir($cntHnd);
    }
}

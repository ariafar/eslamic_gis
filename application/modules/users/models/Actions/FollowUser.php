<?php
class Users_Model_Actions_FollowUser extends Tea_Model_Actions_Abstract
{
    public function render($activity)
    {
        $dt = $activity->getCreationDate();
        if ($dt instanceof DateTime) {
            $dt = $dt->format('Y-m-d H:i:s');
        } else {
            $dt = (string)$dt;
        }

        return 'Follow at ' . $dt;
    }
    
    public function getNotificationMessage($activity)
    {
        $actor = $activity->getActor();
        if (!$actor instanceof Users_Model_User) {
            return false;
        }

        return $actor->getFirstName() . ' ' . $actor->getLastname() . ' Followed you';
    }

    public function getFollowers($activity)
    {
        $followService = Users_Service_Follow::getInstance();
        $followersOfActor = $followService->getUserFollowersIds($activity->getActorId());
        
        $uids = array(
            $activity->getActorId(),
            $activity->getRefId1()
        );
        $uids = array_merge($uids, $followersOfActor);

        return array_unique($uids);
    }
    
    public function getNotifReceivers($activity)
    {
        $uids = array(
            $activity->getRefId1()
        );

        $uids = array_unique($uids);
        if (array_search($activity->getActorId(), $uids) !== false) {
            unset($uids[$activity->getActorId()]);
        }
        
        return $uids;
    }
}

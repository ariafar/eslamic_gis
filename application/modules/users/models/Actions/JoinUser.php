<?php
class Users_Model_Actions_JoinUser extends Tea_Model_Actions_Abstract
{
    public function render($activity)
    {
        $dt = $activity->getCreationDate();
        if ($dt instanceof DateTime) {
            $dt = $dt->format('Y-m-d H:i:s');
        } else {
            $dt = (string)$dt;
        }

        return 'Join at ' . $dt;
    }
}

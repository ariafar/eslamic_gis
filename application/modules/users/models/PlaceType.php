<?php

class Users_Model_PlaceType extends Tea_Model_Entity
{
    const ACCESS_PUBLIC = 0;
    const ACCESS_PRIVATE = 1;

    protected $_properties = array(
        'userId' => null,
        'access' => null,
        'placeTypeId' => null
    );

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'placeTypeId':
                case 'access':
                case 'userId':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}

SET FOREIGN_KEY_CHECKS=0;


--
-- Table structure for table `user_collections`
--

DROP TABLE IF EXISTS `user_collections`;
CREATE TABLE IF NOT EXISTS `user_collections` (
    `id`             BIGINT(20)                  UNSIGNED NOT NULL AUTO_INCREMENT,
    `user_id`        BIGINT(20)                  UNSIGNED NOT NULL,
    `title`          VARCHAR(128)                NOT NULL,
    `role`           VARCHAR(100)                NULL,
    `description`    VARCHAR(128)                NULL,
    `privacy`        ENUM('PRIVATE', 'PUBLIC')   DEFAULT 'PUBLIC',
    `created_by_id`  BIGINT(20)                  UNSIGNED NOT NULL,
    `updated_by_id`  BIGINT(20)                  UNSIGNED NULL,
    `creation_date`  DATETIME                    NOT NULL,
    `update_date`    DATETIME                    NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;

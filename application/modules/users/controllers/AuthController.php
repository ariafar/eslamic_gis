<?php

class Users_AuthController extends Tea_Controller_Action
{

    public function init()
    {
//        if (Zend_Auth::getInstance()->hasIdentity()
//                && $this->_request->getActionName() !== 'logout'
//                && $this->_request->getActionName() !== 'google-accept'
//                && $this->_request->getActionName() !== 'twitter-callback'
//                && $this->_request->getActionName() !== 'denied'
//        ) {
//            $config = Zend_Registry::get('config');
//            $this->_redirect($config->app->url);
//            return;
//        }

        if (Zend_Auth::getInstance()->hasIdentity() && $this->_request->getActionName() !== 'login') {
            Users_Service_Auth::getInstance()->clearCurrentUser();
        }

        $contextSwitch = $this->_helper->getHelper('contextSwitch');
        $contextSwitch->addActionContext('login', array('ajax', 'json'))
                ->addActionContext('denied', array('ajax', 'json'))
                ->addActionContext('logout', array('ajax', 'json'))
                ->addActionContext('dologin', array('ajax', 'json'))
                ->initContext();
    }

    public function loginAction()
    {
        $authService = Users_Service_Auth::getInstance();
        $regionService = Users_Service_Country::getInstance();

        if (!$this->_request->isPost()) {
            return;
        }
        $username = $this->_getParam('username');
        $password = $this->_getParam('password');

        if (empty($username) || empty($password)) {
            $this->view->found = false;
            return;
        }

        $validation = $authService->authenticate($username, $password);

        if ($validation == false) {
            $this->view->found = false;
            return;
        }

        $authService->saveCurrentUser();
        $user = $authService->getCurrentUser();

        $token_val = sprintf('%s-%s-%s', substr(sha1(microtime()), 0, 15), substr(md5(microtime()), 0, 15), uniqid());
        $token = new Users_Model_Token();
        $token_service = Users_Service_Token::getInstance();
        $token->setToken($token_val);
        $token->setUserId($user->getId());
        $token_service->save($token);

//        $current_user = $user->toArray(false, null, array('password'));
        $current_user["token"] = $token->toArray();

        $this->view->found = true;
        $this->view->userId = $user->getId();
        $this->view->token = $token->getToken();
    }

    public function dologinAction()
    {
        $authService = Users_Service_Auth::getInstance();
        $regionService = Users_Service_Country::getInstance();

        if (!$this->_request->isPost()) {
            return;
        }
        $username = $this->_getParam('username');
        $password = $this->_getParam('password');

        if (empty($username) || empty($password)) {
            $this->view->found = false;
            return;
        }

        $validation = $authService->authenticate($username, $password);

        if ($validation == false) {
            $this->view->found = false;
            return;
        }

        $authService->saveCurrentUser();
        $user = $authService->getCurrentUser();

        $current_user = $user->toArray(false, null, array('password'));
        $this->view->found = true;
        $this->view->user = $current_user;
    }

    public function logoutAction()
    {
        Users_Service_Auth::getInstance()->clearCurrentUser();
        $config = Zend_Registry::get('config');
        $this->_redirect($config->app->url, array('prependBase' => false));
    }

    public function userToArray(Users_Model_User $user)
    {
        $_user = $user->toArray();
        $_user['creation_date'] = $user->getCreationDate()->format('Y-m-d H:i');
        $_user['update_date'] = $user->getUpdateDate()->format('Y-m-d H:i');
        if (isset($_user['photo_id'])) {
            $_user['photo_url'] = $this->view->url(
                    array(
                'id' => $_user['photo_id']
                    ), 'resource'
            );
        }
        $_user['profile_url'] = $this->view->url(
                array(
            'controller' => 'auth',
            'module' => 'users',
            'action' => 'profile',
            'id' => $_user['id']
                ), 'default'
        );
        return $_user;
    }

}

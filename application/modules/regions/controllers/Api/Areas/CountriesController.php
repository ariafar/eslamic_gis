<?php

class Regions_Api_Areas_CountriesController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $regionsService = Regions_Service_Region::getInstance();
        $areaService = Regions_Service_RegionArea::getInstance();
        $commentCountrieService = Regions_Service_RegionAreaCountrie::getInstance();

        $activityId = $this->_getParam('id');
        $activity = $regionsService->getByPK($activityId);
        if (!$activity instanceof Regions_Model_Region) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Region)");
            return;
        }

        $commentId = $this->_getParam('sub_id');
        $comment = $areaService->getByPK($commentId);
        if (!$comment instanceof Regions_Model_RegionArea) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Area)");
            return;
        }
        if ($comment->getRegionId() != $activity->getId()) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Area Not For This Region");
            return;
        }

        $limit = $this->_getParam('limit', 10);
        $start = $this->_getParam('start', 0);
        $orderBy = $this->_getParam('order_by', 'creation_date');
        $orderDir = $this->_getParam('order_dir', 'desc');

        $filter = array(
            'comment_id' => $comment->getId()
        );
        $sort = array($orderBy => $orderDir);

        $likes = $commentCountrieService->getList($filter, $sort, $start, $count, $limit);

        $list = array();
        foreach ($likes as $like) {
            $list[] = $like->toArray();
        }

        $result = array(
            'count' => $count,
            'list' => $list
        );

        $response = json_encode($result);
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody($response);
    }

    public function postAction()
    {
        $regionsService = Regions_Service_Region::getInstance();
        $areasService = Regions_Service_Area::getInstance();
        $countriesService = Regions_Service_Country::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return;
        }

        $regionId = $this->_getParam('id');
        $region = $regionsService->getByPK($regionId);
        if (!$region instanceof Regions_Model_Region) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Region)");
            return;
        }

        $areaId = $this->_getParam('sub_id');
        $area = $areasService->getByPK($areaId);
        if (!$area instanceof Regions_Model_Area) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Area)");
            return;
        }

        if ($area->getRegionId() != $region->getId()) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Area Not For This Region");
            return;
        }

        $params = $this->getParams();
        unset($params['id']);

        $country = new Regions_Model_Country();
        $country->fill($params);
        $country->setRegionId($region->getId());
        $country->setAreaId($area->getId());
        $country->setNew(true);
        $_country = $countriesService->save($country);
        if ($_country) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($_country->toArray()));
        }
    }

    public function deleteAction()
    {
        $regionService = Regions_Service_Region::getInstance();
        $areaService = Regions_Service_Area::getInstance();
        $countryService = Regions_Service_Country::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return;
        }

        $regionId = $this->_getParam('id');
        $region = $regionService->getByPK($regionId);
        if (!$region instanceof Regions_Model_Region) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Region)");
            return;
        }

        $areaId = $this->_getParam('sub_id');
        $area = $areaService->getByPK($areaId);
        if (!$area instanceof Regions_Model_Area) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Area)");
            return;
        }
        if ($area->getRegionId() != $region->getId()) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Area Not For This Region");
            return;
        }

        $countryId = $this->_getParam('sub2_id');
        $county = $countryService->getByPK($countryId);
        if (!$county instanceof Regions_Model_Country) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Country)");
            return;
        }

        try {
            $countryService->remove($county);
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode("Success"));
        } catch (\Exception $e) {
            $this->getResponse()
                    ->setHttpResponseCode(400)
                    ->appendBody(json_encode("Error"));
        }
    }

    public function putAction()
    {
        $regionService = Regions_Service_Region::getInstance();
        $areaService = Regions_Service_Area::getInstance();
        $countryService = Regions_Service_Country::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return;
        }

        $regionId = $this->_getParam('id');
        $region = $regionService->getByPK($regionId);
        if (!$region instanceof Regions_Model_Region) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Region)");
            return;
        }

        $areaId = $this->_getParam('sub_id');
        $area = $areaService->getByPK($areaId);
        if (!$area instanceof Regions_Model_Area) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Area)");
            return;
        }
        if ($area->getRegionId() != $region->getId()) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Area Not For This Region");
            return;
        }

        $countryId = $this->_getParam('sub2_id');
        $county = $countryService->getByPK($countryId);
        if (!$county instanceof Regions_Model_Country) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Country)");
            return;
        }

        $params = $this->getParams();
        $county->fill($params);
        $county->setUpdateDate('now');
        $county->setNew(false);
        $service = $countryService->save($county);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($county->toArray()));
    }

}

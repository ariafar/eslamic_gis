<?php

class Regions_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        if (!isset($this->_currentUser)) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody('Unauthorized');
            return;
        }

        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted', 0);

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();
        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($type) && $filter["type"] = $type;
        isset($deleted) && $filter["deleted"] = $deleted;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $regions_service = Regions_Service_Region::getInstance();
        $regions = $regions_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($regions as $item) {
            $item = $item->toArray();
            $areas_service = Regions_Service_Area::getInstance();
            $area_filter = array(
                "regionId" => $item["id"],
                "deleted" => $deleted
            );
            $areas_result = $areas_service->getList($area_filter, $sort);
            $areas = array();

            foreach ($areas_result as $area) {
                $area = $area->toArray();
                $country_service = Regions_Service_Country::getInstance();
                $country_filter = array(
                    "regionId" => $item["id"],
                    "areaId" => $area["id"],
                    "deleted" => $deleted);

                $country_result = $country_service->getList($country_filter, $sort);
                $countries = array();

                foreach ($country_result as $country) {
                    $country = $country->toArray();
                    $countries[] = $country;
                }
                $area["countries"] = $countries;
                $areas[] = $area;
            }
            $item['areas'] = $areas;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'items' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function postAction()
    {
        $params = $this->getParams();

        if (!isset($this->_currentUser)) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody('Unauthorized');
            return;
        }

        $region = new Regions_Model_Region();
        $region->fill($params);

        $result = Regions_Service_Region::getInstance()
                ->save($region);

        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function deleteAction()
    {
        $regionService = Regions_Service_Region::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return;
        }

        $regionId = $this->_getParam('id');
        $region = $regionService->getByPK($regionId);
        if (!$region instanceof Regions_Model_Region) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Region)");
            return;
        }

        try {
            $regionService->remove($region);
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode("Success"));
        } catch (\Exception $e) {
            $this->getResponse()
                    ->setHttpResponseCode(400)
                    ->appendBody(json_encode("Error"));
        }
    }

    public function putAction()
    {

        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return;
        }

        $service = Regions_Service_Region::getInstance();
        $params = $this->getParams();

        $item = $service->getByPK($params['id']);
        //TO Do
        if (!$item instanceof Regions_Model_Region) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Region)');
            return;
        }

        $item->fill($params);
        $item->setUpdateDate('now');
        $item->setNew(false);
        $service = $service->save($item);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($item->toArray()));
    }

}

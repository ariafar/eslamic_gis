<?php

class Feeds_Hook_PerishFeed
{
    public function execute($event, $data)
    {
        $data = array(
            'event' => $event,
            'data'  => $data
        );

        Tea_Service_Gearman::getInstance()->doBackground('feeds', 'perish_feed', $data);
    }
}

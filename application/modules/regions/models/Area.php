<?php

class Regions_Model_Area extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => null,
        'name' => null,
        'regionId' => null,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id':
                case 'name':
                case 'regionId':
                case 'creationDate':
                case 'updateDate':
                case 'deleted':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

    public function setAuthor(Users_Model_User $author)
    {
        $this->_author = $author;
        $this->_properties['author_id'] = $author->getId();
    }

    public function getAuthor()
    {
        if (!isset($this->_author)) {
            $this->_author = Users_Service_User::getInstance()
                    ->getByPK($this->_properties['author_id']);
        }

        return $this->_author;
    }

}

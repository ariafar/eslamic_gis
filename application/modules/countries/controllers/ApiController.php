<?php

class Countries_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $updateDate = $this->getParam("updateDate");
        $deleted = $this->_getParam('deleted', 0);

        $areaId = $this->_getParam('areaId');
        $regionId = $this->_getParam('regionId');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $filter = array();
        isset($updateDate) && $filter["updateDate"] = $updateDate;
        isset($type) && $filter["type"] = $type;
        isset($deleted) && $filter["deleted"] = $deleted;
        isset($areaId) && $filter["areaId"] = $areaId;
        isset($regionId) && $filter["regionId"] = $regionId;

        $dt = new DateTime($str);
        $dt->setTimeZone(new DateTimeZone('Asia/Tehran'));

        $countries_service = Countries_Service_Country::getInstance();
        $countries = $countries_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($countries as $item) {
            $item = $item->toArray();
            $item['areas'] = $areas;
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'items' => $list,
            'updateDate' => $dt->format('Y-m-d H:i:s')
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function postAction()
    {
        $params = $this->getParams();

        if (!isset($this->_currentUser)) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody('Unauthorized');
            return;
        }

        $region = new Countries_Model_Country();
        $region->fill($params);

        $result = Countries_Service_Country::getInstance()
                ->save($region);

        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function deleteAction()
    {
        $regionService = Countries_Service_Country::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return;
        }

        $regionId = $this->_getParam('id');
        $region = $regionService->getByPK($regionId);
        if (!$region instanceof Countries_Model_Country) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Country)");
            return;
        }

        $region->setDeleted(true);
        $region->setUpdateDate('now');
        $region->setNew(false);
        $region = $regionService->save($region);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}

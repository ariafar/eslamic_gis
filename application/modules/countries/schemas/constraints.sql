

ALTER TABLE activities
ADD FOREIGN KEY (`actor_id`) REFERENCES users(`id`);


ALTER TABLE activity_comments
ADD FOREIGN KEY (`activity_id`) REFERENCES activities(`id`),    
ADD FOREIGN KEY (`author_id`)   REFERENCES activities(`actor_id`);


ALTER TABLE activity_comment_likes
ADD FOREIGN KEY (`comment_id`)  REFERENCES activity_comments(`id`),    
ADD FOREIGN KEY (`liker_id`)    REFERENCES users(`id`),
ADD FOREIGN KEY (`activity_id`) REFERENCES activity_comments(`activity_id`);


ALTER TABLE feeds
ADD FOREIGN KEY (`user_id`)         REFERENCES users(`id`),
ADD FOREIGN KEY (`activity_id`)     REFERENCES activities(`id`),
ADD FOREIGN KEY (`created_by_id`)   REFERENCES users(`id`),
ADD FOREIGN KEY (`updated_by_id`)   REFERENCES users(`id`);


ALTER TABLE notifications
ADD FOREIGN KEY (`user_id`)         REFERENCES users(`id`),
ADD FOREIGN KEY (`activity_id`)     REFERENCES activities(`id`),
ADD FOREIGN KEY (`created_by_id`)   REFERENCES users(`id`);


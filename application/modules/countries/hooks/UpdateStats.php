<?php

class Feeds_Hook_UpdateStats
{
    public function execute($event, $data)
    {
        switch ($event) {
        case 'vote_object':
        case 'unvote_object':
            $data = array(
                'event' => $event,
                'data'  => $data
            );

            Tea_Service_Gearman::getInstance()->doBackground('feeds', 'update_stats', $data);
            break;
        }
    }
}

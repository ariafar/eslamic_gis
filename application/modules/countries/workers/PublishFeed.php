<?php

class Feeds_Worker_PublishFeed
{
    public function work($data)
    {
        $event  = $data['event'];
        $object = $data['data'];

        if (    $event != 'create_activity'
            || !is_a($object, 'Feeds_Model_Activity')
        ) {
            return;
        }

        $this->_publishActivity($object);
        $this->_sendNotifications($object);
        $this->_sendEmail($object);
        $this->_sendSocialStatus($object);
    }

    private function _publishActivity($activity)
    {
        $privacy = $activity->getPrivacy();
        $users   = array();

        if (   ($privacy & Feeds_Service_Privacy::FOLLOWERS)
            || ($privacy & Feeds_Service_Privacy::EVERYONE)
        ) {

            /**
             * Fetch Object followers
             */
            $count = 0;
            $start = 0;
            $limit = 100;
            do {
                $_followers = Feeds_Service_Follow::getInstance()
                    ->getFollowers(
                        $activity->getObjectType(),
                        $activity->getObjectId(),
                        null,
                        $start,
                        $count,
                        $limit
                    );
                $users = array_merge($users, $_followers);

                $start += $limit;
            } while ($start < $count);

            /**
             * Fetch actor followers
             */
            $count = 0;
            $start = 0;
            $limit = 100;
            do {
                $_followers = Feeds_Service_Follow::getInstance()
                    ->getFollowers(
                        $activity->getActorType(),
                        $activity->getActorId(),
                        null,
                        $start,
                        $count,
                        $limit
                    );
                $users = array_merge($users, $_followers);

                $start += $limit;
            } while ($start < $count);
        }

        if (empty($users)) {
            return;
        }

        $feedSrv = Feeds_Service_Feed::getInstance();
        foreach ($users as $uid) {
            $feed = new Feeds_Model_Feed();
            $feed->setUserId($uid);
            $feed->setVerb($activity->getVerb());
            $feed->setActivityId($activity->getId());
            $feed->setObjectType($activity->getObjectType());
            $feed->setObjectId($activity->getObjectuId());
            $feed->setPrivacy($activity->getPrivacy());
            $feed->setNew(true);
            $feedSrv->save($feed);
        }
    }

    private function _sendNotifications($activity)
    {
        $verb = $activity->getVerb();
        if (empty($verb)) {
            return false;
        }

        $class = 'Feeds_Model_Verbs_' . ucfirst(strtolower($verb));
        if (!is_callable($class . '::getNotifications')) {
            return false;
        }

        $notifs = $class::getNotifications($activity);
        if (empty($notifs)) {
            return false;
        }

        foreach ($notifs as $notif) {
            $obj = new Feeds_Model_Notification();
            $obj->setActivityId($activity->getId());
            $obj->setType($notif['type']);
            $obj->setToId($notif['to_id']);
            $obj->setFromId($notif['from_id']);
            $obj->setTargetType($notif['target_type']);
            $obj->setTargetId($notif['target_id']);
            $obj->setContextType($notif['context_type']);
            $obj->setContextId($notif['context_id']);
            $obj->setCreationDate($notif['creation_date']);

            Feeds_Service_Notification::getInstance()->save($obj);
        }

        return true;
    }

    private function _sendEmail($activity)
    {
    }

    private function _sendSocialStatus($activity)
    {
    }
}

<?php

class Feeds_Worker_UpdateStats
{
    public function work($data)
    {
        $event  = $data['event'];
        $object = $data['data'];
        
        switch ($event) {
        case 'vote_object':
        case 'unvote_object':
            if (!$object instanceof Feeds_Model_Vote) {
                return;
            }
            $this->_updateVoteCache($object);
            break;
        }
    }

    private function _updateVoteCache(Feeds_Model_Vote $vote)
    {
        $voteService      = Feeds_Service_Vote::getInstance();
        $voteCacheService = Feeds_Service_VoteCache::getInstance();
        
        $filter = array(
            'object_type' => $vote->getObjectType(),
            'object_id'   => $vote->getObjectId()
        );
        
        $types = array(
            Feeds_Model_Vote::TYPE_UP,
            Feeds_Model_Vote::TYPE_DOWN,
            Feeds_Model_Vote::TYPE_SPAM,
            Feeds_Model_Vote::TYPE_RATE
        );
        
        foreach ($types as $type) {
            
            $filter['type'] = $type;
            $voteCache = $voteCacheService->getList($filter, null, 0, $c, 1);
            if ($c == 0) {
                $voteCache = new Feeds_Model_VoteCache();
                $voteCache->setObjectType($vote->getObjectType());
                $voteCache->setObjectId($vote->getObjectId());
                $voteCache->setType($type);
                $voteCache->setNew(true);
            } else {
                $voteCache = $voteCache[0];
                $voteCache->setNew(false);
            }
            
            $voteCount = $voteService->countVotes(
                    $vote->getObjectType(), $vote->getObjectId(), $type);
            
            $voteCache->setValue($voteCount);
            $voteCacheService->save($voteCache);
        }
    }
}

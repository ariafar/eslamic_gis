<?php

class Categories_ApiController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
//        $limit = $this->_getParam('limit', 20);
        $start = $this->_getParam('start', 0);
        $deleted = $this->_getParam('deleted');

        $orderBy = $this->_getParam('order_by', 'updateDate');
        $orderDir = $this->_getParam('order_dir', 'desc');
        $sort = array($orderBy => $orderDir);

        $categories_service = Categories_Service_Category::getInstance();

        $updateDate = $this->_getParam("updateDate");
        $filter = array();

        isset($deleted) && $filter["deleted"] = $deleted;
        isset($updateDate) && $filter["updateDate"] = $updateDate;

        $categories = $categories_service->getList($filter, $sort, $start, $count, $limit);
        $list = array();
        foreach ($categories as $item) {
            $item = $item->toArray();
            $r_service = Resources_Service_Resource::getInstance();
            $image = $r_service->getImagesByParent(array(
                "parentId" => $item["id"],
                "parentType" => "category"
                    ));
            $item["photo"] = $image[0];
            $list[] = $item;
        }

        $response = array(
            'count' => $count,
            'list' => $list
        );
        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($response));
    }

    public function getAction()
    {
        $id = $this->_getParam('id');

        $service = Categories_Service_Category::getInstance();
        $category = $service->getByPK($id);

        if ($category)
            $result = $category->toArray();
        else
            $result = array();

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($result));
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $photo = $params["photo"];

        $user = $authService->getCurrentUser();
        if (!isset($user)) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Unauthorized');
            return;
        }

        $category = new Categories_Model_Category();
        $category->setupdateDate('now');
        $category->fill($params);
        $result = Categories_Service_Category::getInstance()
                ->save($category);

        if ($result) {
            $new_item = $result->toArray();

            if (isset($photo)) {
                $r_model = new Resources_Model_Resource();
                $r_service = Resources_Service_Resource::getInstance();

                $photo['parentId'] = $result->getId();
                $r_model->fill($photo);
                $r_model->setNew(false);
                $r_service->save($r_model);

                $new_item["photo"] = $photo;
            }

            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($new_item));
            return;
        }
    }

    public function putAction()
    {
        $service = Categories_Service_Category::getInstance();
        $params = $this->getParams();
        $photo = $params["photo"];

        $category = $service->getByPK($params['id']);
        //TO Do
        if (!$category instanceof Categories_Model_Category) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Category)');
            return;
        }

        $category->fill($params);
        $category->setUpdateDate('now');
        $category->setNew(false);
        $result = $service->save($category);

        if ($result) {
            $category = $result->toArray();

            if (isset($photo)) {
                $r_model = new Resources_Model_Resource();
                $r_service = Resources_Service_Resource::getInstance();
                $images = $r_service->getImagesByParent(array(
                    "parentId" => $category["id"],
                    "parentType" => "category"
                        ));

                foreach ($images as $img) {
                    $resource = new Resources_Model_Resource();
                    $resource->fill($img);
                    $r_service->remove($resource);
                }

                $photo['parentId'] = $result->getId();
                $r_model->fill($photo);
                $r_model->setNew(true);
                $r_service->save($r_model);


                $category["photo"] = $photo;
            }

            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($category));
            return;
        }
    }

    public function deleteAction()
    {
        $service = Categories_Service_Category::getInstance();
        $categoryId = $this->_getParam('id');
        $category = $service->getByPK($categoryId);

        if (!$category instanceof Categories_Model_Category) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Category)");
            return;
        }

        $service->remove($category);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody("Success");
    }

}


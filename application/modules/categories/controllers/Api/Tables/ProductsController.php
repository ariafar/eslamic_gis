<?php

class Categories_Api_Tables_ProductsController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function postAction()
    {
        $params = $this->getParams();
        $authService = Users_Service_Auth::getInstance();

        $user = $authService->getCurrentUser();
        if (!isset($user)) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Unauthorized');
            return;
        }
        $categoryId = $this->_getParam('id');
        $tableId = $this->_getParam('sub_id');
        $values = $params['values'];

        $product = new Categories_Model_Product();
        $product->setCategoryId($categoryId);
        $product->setTableId($tableId);
        $product->setValues($values);
        $product->setupdateDate('now');

        $result = Categories_Service_Product::getInstance()
                ->save($product);
        if ($result) {
            $this->getResponse()
                    ->setHttpResponseCode(200)
                    ->appendBody(json_encode($result->toArray()));
            return;
        }
    }

    public function putAction()
    {
        $service = Categories_Service_Product::getInstance();
        $params = $this->getParams();

        $product = $service->getByPK($params['id']);
        //TO Do
        if (!$product instanceof Categories_Model_Product) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody('Not Found (Product)');
            return;
        }

        $product->setValues($params['values']);
        $product->setUpdateDate('now');
        $product->setNew(false);
        $product = $service->save($product);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode($product->toArray()));
    }

    public function deleteAction()
    {
        $productService = Categories_Service_Product::getInstance();
        $params = $this->getParams();

        if ($this->_currentUser == null) {
            $this->getResponse()
                    ->setHttpResponseCode(401)
                    ->appendBody("Unauthorized");
            return;
        }

        $product = $productService->getByPK($params["sub2_id"]);
        if (!$product instanceof Categories_Model_Product) {
            $this->getResponse()
                    ->setHttpResponseCode(404)
                    ->appendBody("Not Found (Product)");
            return;
        }

        $productService->remove($product);

        $this->getResponse()
                ->setHttpResponseCode(200)
                ->appendBody(json_encode("success"));
    }

}

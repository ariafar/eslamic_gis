<?php

class External_Model_Product extends Tea_Model_Entity
{

    protected $_properties = array(
        'productCode' => NULL,
        'productTitle' => NULL,
        'unitCode' => NULL,
        'groupId' => NULL,
        'departmentId' => NULL,
        'typeId' => NULL,
        'itemId' => NULL,
        'productSource' => NULL,
        'ptCode' => NULL,
        'updateDate' => NULL,
        'creationDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'productCode' :
                case 'productTitle' :
                case 'unitCode':
                case 'groupId' :
                case 'departmentId' :
                case 'typeId' :
                case 'itemId' :
                case 'productSource' :
                case 'ptCode':
                case 'updateDate' :
                case 'creationDate' :
                case 'deleted' :
                $this->_properties[$key] = $value;
                break;
            }
        }
    }

}


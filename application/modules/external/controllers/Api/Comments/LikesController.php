<?php

class Feeds_Api_Comments_LikesController extends Tea_Controller_Rest_Action
{

    public function init()
    {
        parent::init();
    }

    public function indexAction()
    {
        $activityService    = Feeds_Service_Activity::getInstance();
        $commentService     = Feeds_Service_ActivityComment::getInstance();
        $commentLikeService = Feeds_Service_ActivityCommentLike::getInstance();

        $activityId = $this->_getParam('id');
        $activity   = $activityService->getByPK($activityId);
        if (!$activity instanceof Feeds_Model_Activity) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Activity)");
            return;
        }

        $commentId = $this->_getParam('sub_id');
        $comment   = $commentService->getByPK($commentId);
        if (!$comment instanceof Feeds_Model_ActivityComment) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Comment)");
            return;
        }
        if ($comment->getActivityId() != $activity->getId()) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Comment Not For This Activity");
            return;
        }

        $limit    = $this->_getParam('limit', 10);
        $start    = $this->_getParam('start', 0);
        $orderBy  = $this->_getParam('order_by', 'creation_date');
        $orderDir = $this->_getParam('order_dir', 'desc');

        $filter  = array(
            'comment_id' => $comment->getId()
        );
        $sort = array($orderBy => $orderDir);

        $likes = $commentLikeService->getList($filter, $sort, $start, $count, $limit);

        $list = array();
        foreach ($likes as $like) {
            $list[] = $like->toArray();
        }

        $result = array(
            'count' => $count,
            'list'  => $list
        );

        $response = json_encode($result);
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody($response);
    }

    public function postAction()
    {
        $activityService    = Feeds_Service_Activity::getInstance();
        $commentService     = Feeds_Service_ActivityComment::getInstance();
        $commentLikeService = Feeds_Service_ActivityCommentLike::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                ->setHttpResponseCode(401)
                ->appendBody("Unauthorized");
            return;
        }

        $activityId = $this->_getParam('id');
        $activity   = $activityService->getByPK($activityId);
        if (!$activity instanceof Feeds_Model_Activity) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Activity)");
            return;
        }

        $commentId = $this->_getParam('sub_id');
        $comment   = $commentService->getByPK($commentId);
        if (!$comment instanceof Feeds_Model_ActivityComment) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Comment)");
            return;
        }
        if ($comment->getActivityId() != $activity->getId()) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Comment Not For This Activity");
            return;
        }

        $commentLike = $commentLikeService->getByPK($comment->getId(), $this->_currentUser->getId());
        if ($commentLike instanceof Feeds_Model_ActivityCommentLike) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("already liked");
            return;
        }

        $commentLike = new Feeds_Model_ActivityCommentLike();
        $commentLike->setCommentId($comment->getId());
        $commentLike->setLikerId($this->_currentUser->getId());
        $commentLike->setActivityId($activity->getId());
        $commentLike->setNew(true);
        $commentLike = $commentLikeService->save($commentLike);

        $response = json_encode($commentLike->toArray());
        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody($response);
    }

    public function deleteAction()
    {
        $activityService    = Feeds_Service_Activity::getInstance();
        $commentService     = Feeds_Service_ActivityComment::getInstance();
        $commentLikeService = Feeds_Service_ActivityCommentLike::getInstance();

        if ($this->_currentUser == null) {
            $this->getResponse()
                ->setHttpResponseCode(401)
                ->appendBody("Unauthorized");
            return;
        }

        $activityId = $this->_getParam('id');
        $activity   = $activityService->getByPK($activityId);
        if (!$activity instanceof Feeds_Model_Activity) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Activity)");
            return;
        }

        $commentId = $this->_getParam('sub_id');
        $comment   = $commentService->getByPK($commentId);
        if (!$comment instanceof Feeds_Model_ActivityComment) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Not Found (Comment)");
            return;
        }
        if ($comment->getActivityId() != $activity->getId()) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("Comment Not For This Activity");
            return;
        }

        $commentLike = $commentLikeService->getByPK($comment->getId(), $this->_currentUser->getId());
        if (!$commentLike instanceof Feeds_Model_ActivityCommentLike) {
            $this->getResponse()
                ->setHttpResponseCode(404)
                ->appendBody("already unliked");
            return;
        }

        $commentLikeService->remove($commentLike);

        $this->getResponse()
            ->setHttpResponseCode(200)
            ->appendBody("Success");
    }

}

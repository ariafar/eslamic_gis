<?php

class Activities_Model_Activity extends Tea_Model_Entity
{

    const ACTION_CREATE = 'CREATE';
    const ACTION_UPDATE = 'UPDATE';
    const ACTION_DELETE = 'DELETE';

    protected $_properties = array(
        'id' => NULL,
        'target' => NULL,
        'action' => NULL,
        'userId' => NULL,
        'itemId' => NULL,
        'item' => NULL,
        'creationDate' => NULL,
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id' :
                case 'target':
                case 'action' :
                case 'userId' :
                case 'itemId' :
                case 'item' :
                case 'creationDate' :
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}


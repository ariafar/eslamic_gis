<?php

class PlaceTypes_Model_DbTable_PlaceTypes extends Zend_Db_Table_Abstract
{
    protected $_name    = 'place_types';
    protected $_primary = 'id';
}

<?php

class PlaceTypes_Model_PlaceType extends Tea_Model_Entity
{

    protected $_properties = array(
        'id' => NULL,
        'name' => NULL,
        'icon' => NULL,
        'order' => NULL,
        'creationDate' => NULL,
        'updateDate' => NULL,
        'deleted' => 0
    );

    public function __construct()
    {
        parent::__construct();

        $this->setCreationDate('now');
        $this->setUpdateDate('now');
    }

    public function fill($record)
    {
        foreach ($record as $key => $value) {
            switch ($key) {
                case 'id':
                case 'name' :
                case 'icon' :
                case 'order':
                case 'creationDate':
                case 'updateDate':
                case 'deleted':
                    $this->_properties[$key] = $value;
                    break;
            }
        }
    }

}
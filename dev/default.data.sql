SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `places`;
CREATE TABLE IF NOT EXISTS `places` (
    `id`                INT(20)  UNSIGNED   NOT NULL AUTO_INCREMENT,
    `name`              VARCHAR(500)        NOT NULL,
    `localName`         VARCHAR(500)            NULL,
    `description`       TEXT                    NULL,
    `countryId`         INT(20)  UNSIGNED       NOT NULL,
    `typeId`            INT(20)  UNSIGNED       NOT NULL,
    `latitude`          VARCHAR (15)            NULL,
    `longitude`         VARCHAR (15)            NULL,
    `photo`             VARCHAR (500)           NULL,
    `email`             VARCHAR (100)           NULL,
    `website`           VARCHAR (100)           NULL,
    `tell`              VARCHAR (100)           NULL,
    `status`            TINYINT             NOT NULL,
    `isPrivate`          BOOLEAN                 NULL,
    `managerName`       VARCHAR (500)           NULL,
    `acceptBy`          INT(20) UNSIGNED        NULL,
    `createBy`          INT(20) UNSIGNED        NOT NULL,
    `creationDate`      DATETIME                NULL,
    `updateDate`        DATETIME                NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;



DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
    `id`                INT(20)  UNSIGNED   NOT NULL AUTO_INCREMENT,
    `username`          VARCHAR(50)         NOT NULL UNIQUE,
    `password`          VARCHAR(100)        NOT NULL,
    `firstName`         VARCHAR(50)         NOT NULL,
    `lastName`          VARCHAR(50)         NOT NULL,
    `photo`             VARCHAR (500)       NOT NULL,
    `email`             VARCHAR (100)           NULL,
    `tell`              VARCHAR (100)           NULL,
    `permission`        TINYINT             NOT NULL,
    `type`              TINYINT             NOT NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


INSERT INTO `users` (`id`, `username`, `password`, `firstName`, `lastName`, `photo`, `email`, `tell`, `permission`, `type`, `creationDate`, `updateDate`, `deleted`) VALUES
(1, 'admin', '123456', '', '', '', NULL, NULL, 1, 1, '2015-02-15 02:53:03', '2015-02-15 02:53:03', 0);


DROP TABLE IF EXISTS `user_tokens`;
CREATE TABLE IF NOT EXISTS `user_tokens` (
    `id`                INT(20)  UNSIGNED   NOT NULL AUTO_INCREMENT,
    `userId`            VARCHAR(50)         NOT NULL,
    `token`             VARCHAR(100)        NOT NULL,
    `creationDate`      DATETIME            NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;


DROP TABLE IF EXISTS `place_types`;
CREATE TABLE IF NOT EXISTS `place_types` (
    `id`                INT(20)  UNSIGNED   NOT NULL AUTO_INCREMENT,
    `name`              VARCHAR(50)         NOT NULL UNIQUE,
    `icon`              VARCHAR (500)       NOT NULL,
    `order`             TINYINT             NOT NULL,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;



DROP TABLE IF EXISTS `regions`;
CREATE TABLE IF NOT EXISTS `regions` (
    `id`                INT(20)  UNSIGNED   NOT NULL AUTO_INCREMENT,
    `name`              VARCHAR(50)         NOT NULL UNIQUE,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;



DROP TABLE IF EXISTS `areas`;
CREATE TABLE IF NOT EXISTS `areas` (
    `id`                INT(20)  UNSIGNED   NOT NULL AUTO_INCREMENT,
    `regionId`          INT(20)  UNSIGNED   NOT NULL,
    `name`              VARCHAR(50)         NOT NULL UNIQUE,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;



DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
    `id`                INT(20)  UNSIGNED   NOT NULL AUTO_INCREMENT,
    `regionId`          INT(20)  UNSIGNED   NOT NULL,
    `areaId`            INT(20)  UNSIGNED   NOT NULL,
    `name`              VARCHAR(50)         NOT NULL UNIQUE,
    `creationDate`      DATETIME            NULL,
    `updateDate`        DATETIME            NULL,
    `deleted`           BOOLEAN NOT NULL DEFAULT FALSE,
    PRIMARY KEY (`id`)

) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=1 ;




DROP TABLE IF EXISTS `user_countries`;
CREATE TABLE IF NOT EXISTS `user_countries` (
    `countryId`          INT(20)   UNSIGNED  NOT NULL,
    `userId`             INT(20)   UNSIGNED  NOT NULL,
    `write`              TINYINT(2)          NOT NULL,

    PRIMARY KEY (`userId`,`countryId`),
    KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;



DROP TABLE IF EXISTS `user_placeTypes`;
CREATE TABLE IF NOT EXISTS `user_placeTypes` (
    `placeTypeId`        INT(20) UNSIGNED    NOT NULL,
    `access`             TINYINT(2)          NOT NULL,
    `userId`             INT(20) UNSIGNED    NOT NULL,

    PRIMARY KEY (`userId`,`placeTypeId`),
    KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;




SET FOREIGN_KEY_CHECKS=1; 


--
-- Constraints for table `places`
--
ALTER TABLE `places`
    ADD CONSTRAINT `place_country_fk` FOREIGN KEY (`countryId`) REFERENCES `countries` (`id`) ON DELETE RESTRICT,
    ADD CONSTRAINT `place_type_fk` FOREIGN KEY (`typeId`) REFERENCES `place_types` (`id`) ON DELETE RESTRICT,
    ADD CONSTRAINT `place_user_fk1` FOREIGN KEY (`acceptBy`) REFERENCES `users` (`id`) ON DELETE RESTRICT,
    ADD CONSTRAINT `place_user_fk2` FOREIGN KEY (`createBy`) REFERENCES `users` (`id`) ON DELETE RESTRICT;



--
-- Constraints for table `areas`
--
ALTER TABLE `areas`
     ADD FOREIGN KEY (regionId) REFERENCES regions(id)
     ON DELETE RESTRICT;


--
-- Constraints for table `countries`
--
ALTER TABLE `countries`
    ADD FOREIGN KEY (regionId) REFERENCES regions(id) ON DELETE RESTRICT,
    ADD FOREIGN KEY (areaId) REFERENCES areas(id) ON DELETE RESTRICT;

--
-- Constraints for table `user_countries`
--
ALTER TABLE `user_countries`
    ADD FOREIGN KEY (countryId) REFERENCES countries(id) ON DELETE RESTRICT,
     ADD FOREIGN KEY (userId) REFERENCES users(id) ON DELETE RESTRICT;

--
-- Constraints for table `user_placeTypes`
--
ALTER TABLE `user_placeTypes`
    ADD FOREIGN KEY (userId) REFERENCES users(id) ON DELETE RESTRICT,
    ADD FOREIGN KEY (placeTypeId) REFERENCES place_types(id) ON DELETE RESTRICT;

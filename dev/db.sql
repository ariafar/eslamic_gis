-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 15, 2015 at 02:12 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.26

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tes_eslami`
--

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE IF NOT EXISTS `areas` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `regionId` int(20) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `creationDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `regionId` (`regionId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `regionId`, `name`, `creationDate`, `updateDate`, `deleted`) VALUES
(1, 1, 'شبه قاره', '2015-03-08 06:30:01', '2015-03-15 15:11:40', 0),
(2, 2, 'اروپای شرقی', '2015-03-08 07:42:05', NULL, 0),
(3, 2, 'اروپای غربی', '2015-03-15 14:30:22', NULL, 0),
(4, 2, 'آمریکا', '2015-03-15 15:08:38', NULL, 0),
(8, 1, 'شرق آسیا', '2015-03-15 15:13:15', NULL, 0),
(9, 1, 'آسیای مرکزی و قفقاز', '2015-03-15 15:14:02', NULL, 0),
(10, 3, 'شرق و جنوب آفریقا', '2015-03-15 15:17:10', NULL, 0),
(11, 3, 'غرب و مرکز و آفریقا', '2015-03-15 15:17:58', NULL, 0),
(12, 3, 'جوزه عربی', '2015-03-15 15:18:48', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `regionId` int(20) unsigned NOT NULL,
  `areaId` int(20) unsigned NOT NULL,
  `name` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `creationDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `regionId` (`regionId`),
  KEY `areaId` (`areaId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=33 ;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `regionId`, `areaId`, `name`, `creationDate`, `updateDate`, `deleted`) VALUES
(1, 1, 1, 'پاکستان', '2015-03-08 06:30:10', NULL, 0),
(2, 2, 2, 'صربستان', '2015-03-08 07:42:18', NULL, 0),
(3, 2, 2, 'کرواسی', '2015-03-08 07:47:13', NULL, 0),
(4, 2, 3, 'یونان', '2015-03-15 14:30:34', '2015-03-15 14:30:53', 0),
(7, 2, 3, 'سوئد', '2015-03-15 14:31:26', NULL, 0),
(8, 2, 3, 'آلمان', '2015-03-15 14:31:43', NULL, 0),
(9, 2, 3, 'فرانسه', '2015-03-15 14:32:01', NULL, 0),
(10, 2, 3, 'ایتالیا', '2015-03-15 14:32:15', NULL, 0),
(11, 2, 3, 'اسپانیا', '2015-03-15 14:32:37', NULL, 0),
(12, 2, 2, 'تاتارستان', '2015-03-15 15:22:17', NULL, 0),
(13, 2, 2, 'بلغارستان', '2015-03-15 15:22:55', NULL, 0),
(14, 2, 2, 'روسیه', '2015-03-15 15:23:31', NULL, 0),
(15, 2, 2, 'بوسنی', '2015-03-15 15:24:17', NULL, 0),
(16, 2, 2, 'آلبانی', '2015-03-15 15:24:56', NULL, 0),
(17, 2, 2, 'بلاروس', '2015-03-15 15:25:33', NULL, 0),
(18, 2, 3, 'اتریش', '2015-03-15 15:26:30', NULL, 0),
(19, 2, 4, 'آمریکا', '2015-03-15 15:27:48', NULL, 0),
(20, 2, 4, 'کانادا', '2015-03-15 15:28:24', NULL, 0),
(21, 1, 1, 'هند', '2015-03-15 15:30:23', NULL, 0),
(22, 1, 1, 'بنگلادش', '2015-03-15 15:31:01', NULL, 0),
(23, 1, 1, 'افغانستان', '2015-03-15 15:31:32', NULL, 0),
(24, 1, 8, 'تایلند', '2015-03-15 15:32:05', NULL, 0),
(25, 1, 8, 'استرالیا', '2015-03-15 15:33:02', NULL, 0),
(26, 1, 8, 'سریلانکا', '2015-03-15 15:33:41', NULL, 0),
(27, 1, 8, 'ژاپن', '2015-03-15 15:34:26', NULL, 0),
(28, 1, 8, 'مالزی', '2015-03-15 15:35:03', NULL, 0),
(29, 1, 8, 'اندوزی', '2015-03-15 15:35:40', NULL, 0),
(30, 1, 8, 'فیلیپین', '2015-03-15 15:36:25', NULL, 0),
(31, 1, 8, 'چین', '2015-03-15 15:37:09', NULL, 0),
(32, 1, 9, 'ایران', '2015-03-15 15:47:05', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE IF NOT EXISTS `places` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) COLLATE utf8_persian_ci NOT NULL,
  `localName` varchar(500) COLLATE utf8_persian_ci DEFAULT NULL,
  `description` text COLLATE utf8_persian_ci,
  `countryId` int(20) unsigned NOT NULL,
  `typeId` int(20) unsigned NOT NULL,
  `latitude` varchar(15) COLLATE utf8_persian_ci DEFAULT NULL,
  `longitude` varchar(15) COLLATE utf8_persian_ci DEFAULT NULL,
  `photo` varchar(500) COLLATE utf8_persian_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `website` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `tell` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `isPrivate` tinyint(1) DEFAULT NULL,
  `managerName` varchar(500) COLLATE utf8_persian_ci DEFAULT NULL,
  `acceptBy` int(20) unsigned DEFAULT NULL,
  `createBy` int(20) unsigned NOT NULL,
  `creationDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `place_country_fk` (`countryId`),
  KEY `place_type_fk` (`typeId`),
  KEY `place_user_fk1` (`acceptBy`),
  KEY `place_user_fk2` (`createBy`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=32 ;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `name`, `localName`, `description`, `countryId`, `typeId`, `latitude`, `longitude`, `photo`, `email`, `website`, `tell`, `status`, `isPrivate`, `managerName`, `acceptBy`, `createBy`, `creationDate`, `updateDate`, `deleted`) VALUES
(2, 'سازمان فرهنگ و ارتباطات اسلامی', 'Islamic Cultural and Relations Organization', NULL, 32, 5, '35.7443475', '51.4272458', '/upfiles/places/1426423523_torkaman.jpg', 'info@icro.ir ', 'www.icro.ir ', '88151', 2, 1, 'دکتر ابوذر ابراهیمی', NULL, 1, '2015-03-08 07:25:02', '2015-03-15 16:39:36', 0),
(21, 'رایزنی فرهنگی ج.ا.ا در یونان', 'Cultural Center of Iran', NULL, 4, 4, '37°57''08.3', '23°45''41.7', '/upfiles/places/1426417506_Athen.jpg', 'mozafari.athens@icro.ir', 'athens.icro.ir', '+30) 2106840244 -2106850318', 2, 1, 'حجه الاسلام والمسلمین مظفری', NULL, 1, '2015-03-15 14:35:06', '2015-03-15 16:41:16', 0),
(22, 'رایزنی فرهنگی ج.ا.ا-برلین', 'Kulturabteilung der Botschaft der Isl. Rep. Iran Drakestr. 3 D-12205 Berlin ', NULL, 8, 4, '52.460980', '13.299919', '/upfiles/places/1426420003_Berlin.jpg', 'info@irankultur.com', 'http://berlin.icro.ir	', 'تلفن: 740715400-4930++ نمابر: 4930-740715419++', 2, 1, 'حجت السلام و المسلمین ایمانی پور', NULL, 1, '2015-03-15 15:16:43', '2015-03-15 15:22:02', 0),
(23, 'رایزنی فرهنگی ج.ا.ا- استکهلم ', 'Kuttervägen1, 4tr  183 53, Täby ‎  Stockholm, Sweden', NULL, 7, 4, '52.460980', '13.299919', '/upfiles/places/1426420626_Stockholm.jpg', 'info@irankultur.com', 'http://berlin.icro.ir', 'تلفن: 740715400-4930++ نمابر: 740715419-4930++', 2, 1, 'جناب آقای دکتر ', NULL, 1, '2015-03-15 15:27:06', '2015-03-15 15:28:45', 0),
(24, 'رایزنی فرهنگی ج.ا.ا- بلگراد', 'Kulturni centar I.R. Irana Neznanog junaka 32 11040 Beograd, Srbija ', NULL, 2, 4, '44.771867', '20.465359', NULL, 'info@iran.rs', 'http://belgrade.icro.ir', '00-381-11-3672564', 2, 1, 'جناب آقابی دکتر شالوئی', NULL, 1, '2015-03-15 16:00:29', '2015-03-15 16:01:20', 0),
(25, 'رایزنی فرهنگی ج.ا.ا-پاریس', 'Centre Culturel Iranien - 6 , rue Jean Bart 75006 Paris', NULL, 9, 4, '48.847848', '2.330912', '/upfiles/places/1426423260_paris.jpg', 'cciran.paris@gmail.com', 'http://paris.icro.ir', 'تلفن 20-19-49-45-1-0033 فاکس 34-31-49-45-1-0033', 2, 1, 'جناب آقابی دکتر ', NULL, 1, '2015-03-15 16:11:00', '2015-03-15 16:11:54', 0),
(26, 'رایزنی فرهنگی ج.ا.ا-تیرانا', 'Rr. Ismail Qemali, Gener. 3, Shk. 1, Tiranë', NULL, 16, 4, '41.321845', ' 19.823409', '/upfiles/places/1426423951_albani-tirana.jpg', 'iranalbania@gmail.com', 'http://www.iranalbania.ir', 'تلفن : +355 4 2400478 نمابر: +355 4 2245613', 2, 1, 'جناب آقای دکتر', NULL, 1, '2015-03-15 16:22:31', '2015-03-15 16:22:44', 0),
(27, 'رایزنی فرهنگی ج.ا.ا-رم', 'VIA MARIA PEZZE PASCOLATO, 9, 00135 ROMA', NULL, 10, 4, '41.938959', '12.432361', '/upfiles/places/1426424414_roum.jpg', 'Istitutoculturaleiran@gmail.com , Rome@icro.ir', 'http:// rome.icro.ir', 'Tel. (+39)06 3052207, 063052208 Fax. (+39)06 3017341', 2, 1, 'جناب آقای قربانعلی پورمرجان', NULL, 1, '2015-03-15 16:30:14', '2015-03-15 16:34:42', 0),
(28, 'رایزنی فرهنگی ج.ا.ا- زاگرب', 'tuskanac 65 - 10000 zagreb', NULL, 3, 4, '45.829741', '15.970988', NULL, 'nfo@iran.hr', 'http://zagreb.icro.ir', 'تلفن : 003851-4834171فاکس : 003851-4834172', 2, 1, 'دکتر', NULL, 1, '2015-03-15 16:33:46', '2015-03-15 16:34:38', 0),
(29, 'رایزنی فرهنگی ج.ا.ا- دهلی نو', 'No. 18, Tilak Marg, New Delhi- 110001 ', NULL, 21, 4, '28.620093', '77.236382', '/upfiles/places/1426424953_dehli.jpg', 'newdelhi@icro.ir ichdelhi@gmail.com', 'http://newdelhi.icro.ir', '0091-11-23383232', 2, 1, 'جناب آقاب فولادی', NULL, 1, '2015-03-15 16:39:13', '2015-03-15 16:39:26', 0),
(30, 'سازمان', '', '', 1, 5, '35.744616', '51.4256525', '/upfiles/places//1426491510_Farayab-20150316_141032.png', 'vhrf@dgh.com', 'www.bgfh.com', '67755', 1, NULL, '', NULL, 1, '2015-03-16 11:08:22', '2015-03-16 11:08:22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `place_types`
--

CREATE TABLE IF NOT EXISTS `place_types` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `icon` varchar(500) COLLATE utf8_persian_ci NOT NULL,
  `order` tinyint(4) NOT NULL,
  `creationDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `place_types`
--

INSERT INTO `place_types` (`id`, `name`, `icon`, `order`, `creationDate`, `updateDate`, `deleted`) VALUES
(1, 'دانشگاه', 'http://185.4.30.211/upfiles/place-types//1426417063_pinmdpi.png', 2, '2015-03-08 06:33:51', '2015-03-16 07:34:14', 0),
(3, 'سینما', 'http://185.4.30.211/upfiles/place-types//1426417041_pinmdpi.png', 4, '2015-03-08 08:42:22', '2015-03-16 07:34:14', 0),
(4, 'نمایندگان فرهنگی ج.ا.ا', 'http://185.4.30.211/upfiles/place-types//1426425377_pinhdpi.png', 11, '2015-03-15 14:26:57', '2015-03-16 07:34:15', 0),
(5, 'دستگاه های دولتی', 'http://185.4.30.211/upfiles/place-types//1426425262_pinhdpi.png', 6, '2015-03-15 14:54:57', '2015-03-16 07:34:15', 0),
(6, 'کلیساها', 'http://185.4.30.211/upfiles/place-types//1426418746_pinmdpi.png', 3, '2015-03-15 14:55:46', '2015-03-16 07:34:14', 0),
(7, 'مساجد شیعی', 'http://185.4.30.211/upfiles/place-types//1426418780_pinmdpi.png', 7, '2015-03-15 14:56:20', '2015-03-16 07:34:15', 0),
(8, 'مساجد اهل تسنن', 'http://185.4.30.211/upfiles/place-types//1426418828_pinmdpi.png', 8, '2015-03-15 14:57:08', '2015-03-16 07:34:15', 0),
(9, 'موزه ها', 'http://185.4.30.211/upfiles/place-types//1426418883_pinmdpi.png', 10, '2015-03-15 14:58:03', '2015-03-16 07:34:16', 0),
(10, 'کتابخانه', 'http://185.4.30.211/upfiles/place-types//1426419024_pinmdpi.png', 10, '2015-03-15 15:00:24', '2015-03-16 07:34:02', 0),
(11, 'آمفی تئاتر', 'http://185.4.30.211/upfiles/place-types//1426419148_pinmdpi.png', 1, '2015-03-15 15:02:28', '2015-03-16 07:34:14', 0),
(12, 'رستوران های حلال', 'http://185.4.30.211/upfiles/place-types//1426422218_pinmdpi.png', 5, '2015-03-15 15:53:38', '2015-03-16 07:34:14', 0);

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `creationDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `name`, `creationDate`, `updateDate`, `deleted`) VALUES
(1, 'آسیا و اقیانوسیه', '2015-03-08 06:29:49', '2015-03-15 15:11:07', 0),
(2, 'اروپا و آمریکا', '2015-03-08 07:41:42', '2015-03-15 15:09:23', 0),
(3, 'آفریقا و عربی', '2015-03-15 15:14:59', '2015-03-15 15:14:59', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `firstName` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `lastName` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `photo` varchar(500) COLLATE utf8_persian_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `tell` varchar(100) COLLATE utf8_persian_ci DEFAULT NULL,
  `permission` tinyint(4) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `creationDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `firstName`, `lastName`, `photo`, `email`, `tell`, `permission`, `type`, `creationDate`, `updateDate`, `deleted`) VALUES
(1, 'admin', '123456', '', '', '', NULL, NULL, 1, 1, '2015-02-15 02:53:03', '2015-02-15 02:53:03', 0),
(2, 'ff', '123456', 'zxdvfv', 'sfvsdfv', '/upfiles/place-types//1425789743_PicS3205.jpg', 'ssdcs', '2323', 2, 2, '2015-03-08 08:12:23', '2015-03-08 08:12:23', 0),
(3, 'test', '123456', 'محمد', 'محمدی', NULL, 'test@yahoo.com', '44557788', 3, 2, '2015-03-16 07:45:06', '2015-03-16 07:45:06', 0),
(4, 'icro', '123456', 'test', 'test', NULL, 'ranic2000@gmail.com ', '6666655', 3, 2, '2015-04-07 19:50:25', '2015-04-07 19:50:25', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_countries`
--

CREATE TABLE IF NOT EXISTS `user_countries` (
  `countryId` int(20) unsigned NOT NULL,
  `userId` int(20) unsigned NOT NULL,
  `write` tinyint(2) NOT NULL,
  PRIMARY KEY (`userId`,`countryId`),
  KEY `userId` (`userId`),
  KEY `countryId` (`countryId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `user_countries`
--

INSERT INTO `user_countries` (`countryId`, `userId`, `write`) VALUES
(2, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_placeTypes`
--

CREATE TABLE IF NOT EXISTS `user_placeTypes` (
  `placeTypeId` int(20) unsigned NOT NULL,
  `access` tinyint(2) NOT NULL,
  `userId` int(20) unsigned NOT NULL,
  PRIMARY KEY (`userId`,`placeTypeId`),
  KEY `userId` (`userId`),
  KEY `placeTypeId` (`placeTypeId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci;

--
-- Dumping data for table `user_placeTypes`
--

INSERT INTO `user_placeTypes` (`placeTypeId`, `access`, `userId`) VALUES
(1, 1, 3),
(3, 1, 3),
(6, 1, 3),
(11, 1, 3),
(12, 1, 3),
(1, 0, 4),
(4, 0, 4),
(6, 0, 4),
(11, 0, 4);

-- --------------------------------------------------------

--
-- Table structure for table `user_tokens`
--

CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `userId` varchar(50) COLLATE utf8_persian_ci NOT NULL,
  `token` varchar(100) COLLATE utf8_persian_ci NOT NULL,
  `creationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_persian_ci AUTO_INCREMENT=61 ;

--
-- Dumping data for table `user_tokens`
--

INSERT INTO `user_tokens` (`id`, `userId`, `token`, `creationDate`) VALUES
(1, '1', '07149db33d078db-3e42f9c97ff4555-54fb3ad102be6', '2015-03-07 21:22:17'),
(2, '1', 'c546e8a372a86bb-8118d81213dbef5-54fbbaa40b983', '2015-03-08 06:27:40'),
(3, '1', '74f00564368b248-a85897ceaadb733-54fbbdf086f01', '2015-03-08 06:41:44'),
(4, '1', 'b6f25bd86228603-9dd84daa39191e6-54fbbf4e0437e', '2015-03-08 06:47:34'),
(5, '1', '900f3c5f9761bcd-798168007446b04-54fbc4ac8827d', '2015-03-08 07:10:28'),
(6, '1', '2a9e63639982226-3eb8e61730afb56-54fbc4ae022d0', '2015-03-08 07:10:30'),
(7, '1', '59ea8b727f78d27-f0dc1721cb2b00b-54fbc76af194a', '2015-03-08 07:22:10'),
(8, '1', 'a222c99799ab991-dc50c618ecb8dbe-54fbc9501c05b', '2015-03-08 07:30:16'),
(9, '1', '26a30a149bca9fd-2f0a6610cd5124a-54fbcba1a4cf9', '2015-03-08 07:40:09'),
(10, '1', '2fbd59ec0f014f4-98560ceb1830838-54fbcbe7d5a99', '2015-03-08 07:41:19'),
(11, '1', '185f7c90919a189-29a58ff9a4ba01c-54fbcfef3c49d', '2015-03-08 07:58:31'),
(12, '1', 'b71f2c1086a373d-250fa225a186a2f-54fbd7ab89274', '2015-03-08 08:31:31'),
(13, '1', '70ebc6deac2e232-8371681b49d1d14-55010b66a4edc', '2015-03-12 07:13:34'),
(14, '1', '6dea46116073bd8-79b519259def839-55010bfd6569b', '2015-03-12 07:16:05'),
(15, '1', '42feecd11ec56f1-4e19a29e6fba0e2-550111726e8ff', '2015-03-12 07:39:22'),
(16, '1', 'da79dd696ccbf10-d50ee2a97ae9858-5501125ce682b', '2015-03-12 07:43:16'),
(17, '1', 'c0f4bf8ac66814f-264c15cba8fceaf-55011baf31d34', '2015-03-12 08:23:03'),
(18, '1', 'd50e748fd66e1c9-172c56a9527733f-55045798b788c', '2015-03-14 19:15:28'),
(19, '1', 'aae7a7dcabc8b8b-35c48074e1a18a4-550458e4e64a0', '2015-03-14 19:21:00'),
(20, '1', '8e201ded690a15d-0eeadaf3f7f3f5a-55046154a9993', '2015-03-14 19:57:00'),
(21, '1', 'b5ffcde861b7525-d4b8b5536db5360-550461e3c3fc8', '2015-03-14 19:59:23'),
(22, '1', '0b1e2ba1680f778-b7d9d13493b4ab9-55047068a2412', '2015-03-14 21:01:20'),
(23, '1', 'df893267de54404-d4728e5cec9f9f8-550478b5b03f5', '2015-03-14 21:36:45'),
(24, '1', 'cc89319dea8fa7a-5d6d3c2a3e5e02c-55050a1671d0e', '2015-03-15 07:57:02'),
(25, '1', '5fe4ea9bdf9c1f8-50d88e21c018ca0-55050c5154cfc', '2015-03-15 08:06:33'),
(26, '1', 'e678f59b59fe56f-b74d7f5a0f16f7f-55051aff48961', '2015-03-15 09:09:11'),
(27, '1', 'af8d3a2a57592bc-fcb034e0f50d931-55051b2b40236', '2015-03-15 09:09:55'),
(28, '1', 'e5ba4a6f8284168-d942cef24d4436d-55052081d45cc', '2015-03-15 09:32:41'),
(29, '1', '01952d9a58c58a2-0547521c83e0c5a-550520cbea7c0', '2015-03-15 09:33:55'),
(30, '1', 'c66973c00e30d93-a86477ce43c1ac7-5505487a2b47a', '2015-03-15 12:23:14'),
(31, '1', '39fb18ba576ae10-efdfcc1fc782285-550549b3eb3d7', '2015-03-15 12:28:27'),
(32, '1', '0047a1c6c236305-202a16159f87f16-5523e32fcfbac', '2015-04-07 18:31:19'),
(33, '1', '77f5be31410e8d9-a2b2b6164e15fd5-5524d3678cb5c', '2015-04-08 11:36:15'),
(34, '1', '8b1d2b7a4060fc8-f8b382e048de103-5524d36e3d4ed', '2015-04-08 11:36:22'),
(35, '1', 'bb39e401a25bb35-02d74b32fdd9a8e-55289c9c57604', '2015-04-11 08:31:32'),
(36, '1', '4ab928e03af1814-b000de052ef87f9-5528a0695e501', '2015-04-11 08:47:45'),
(37, '1', '08d2278c58d5249-4e918bb23bb6490-5528a1dddc46b', '2015-04-11 08:53:57'),
(38, '1', 'bac6f8368dbebfe-5ae5b46058a984b-5528a3e6af17e', '2015-04-11 09:02:38'),
(39, '1', '04a58657cd8ff4d-2a317dc503977ee-5528e020a8903', '2015-04-11 13:19:36'),
(40, '1', '4f0b5f3a1301e57-be1633d39024a50-5528e17e622ec', '2015-04-11 13:25:26'),
(41, '1', '931c1e779b6f465-7edc7582b5e67a5-5528e2c30777d', '2015-04-11 13:30:51'),
(42, '1', '57c81484b60ae34-2d7c3224dfbb30a-5528e36c6adee', '2015-04-11 13:33:40'),
(43, '1', '5990477a0a81b11-07787e13ba1ccf2-5528e392f1003', '2015-04-11 13:34:18'),
(44, '1', 'b9eee393fe3e383-a924886689e3530-5528e40c77924', '2015-04-11 13:36:20'),
(45, '1', '0644712a6fd9e26-4cb9455087e6c3f-5528e5809ee43', '2015-04-11 13:42:32'),
(46, '1', '10cdef6ec69aee6-9553bdf9d6b9120-5528e5c9c6b72', '2015-04-11 13:43:45'),
(47, '1', '97311499728ba6d-9a8881fcaf3b005-5528e9cb099bd', '2015-04-11 14:00:51'),
(48, '1', '97cdcdb1c23370d-ab74d75f7b55c60-5528eaedddf16', '2015-04-11 14:05:41'),
(49, '1', '915d12d5796883c-6eeeecd2ef0b191-5528eb9e64fc4', '2015-04-11 14:08:38'),
(50, '1', '25eb6b3148e9288-c81dd4726bc459a-5528ec177098a', '2015-04-11 14:10:39'),
(51, '1', 'f2229f74acb370c-99774d55a706ac1-5528fdc9d8ef0', '2015-04-11 15:26:09'),
(52, '1', '4bd271634a30b04-4a27992c131cc8b-5529149039a21', '2015-04-11 17:03:20'),
(53, '1', '91e6642901c66e8-de0698efe4b19ea-5529da500ce57', '2015-04-12 07:07:04'),
(54, '1', 'd535138b8e78944-d75847435a039ff-5529da534ffd9', '2015-04-12 07:07:07'),
(55, '1', 'a6186222364996e-dce8f4a233a50ed-5529f59c572df', '2015-04-12 09:03:32'),
(56, '1', 'a4e056a25d11c59-62dc5095b20421e-552c9e2d4f2a5', '2015-04-14 09:27:17'),
(57, '1', 'c7ad84cf69de737-dab2d1bf095c151-552cbbae68b50', '2015-04-14 11:33:10'),
(58, '1', '5c0a9a843d1318a-871a9187bd7bfeb-552cbbf1db822', '2015-04-14 11:34:17'),
(59, '1', 'c00188a186b45a1-d46b8ebc5851917-552cbd68f3dda', '2015-04-14 11:40:32'),
(60, '1', 'b69c4a51ca7f06d-359e7dec528ffa4-552cbf5c6a67c', '2015-04-14 11:48:52');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_ibfk_1` FOREIGN KEY (`regionId`) REFERENCES `regions` (`id`);

--
-- Constraints for table `countries`
--
ALTER TABLE `countries`
  ADD CONSTRAINT `countries_ibfk_1` FOREIGN KEY (`regionId`) REFERENCES `regions` (`id`),
  ADD CONSTRAINT `countries_ibfk_2` FOREIGN KEY (`areaId`) REFERENCES `areas` (`id`);

--
-- Constraints for table `places`
--
ALTER TABLE `places`
  ADD CONSTRAINT `place_country_fk` FOREIGN KEY (`countryId`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `place_type_fk` FOREIGN KEY (`typeId`) REFERENCES `place_types` (`id`),
  ADD CONSTRAINT `place_user_fk1` FOREIGN KEY (`acceptBy`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `place_user_fk2` FOREIGN KEY (`createBy`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_countries`
--
ALTER TABLE `user_countries`
  ADD CONSTRAINT `user_countries_ibfk_1` FOREIGN KEY (`countryId`) REFERENCES `countries` (`id`),
  ADD CONSTRAINT `user_countries_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_placeTypes`
--
ALTER TABLE `user_placeTypes`
  ADD CONSTRAINT `user_placeTypes_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_placeTypes_ibfk_2` FOREIGN KEY (`placeTypeId`) REFERENCES `place_types` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

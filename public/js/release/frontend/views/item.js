
define([
    'jquery',
    'underscore',
    'backbone',
    'base/views/item_view',
    'frontend/models/item',
    'text!frontend/templates/news_item.html',
    'text!frontend/templates/evaluation_item.html',
    'text!frontend/templates/education_item.html',
    'text!frontend/templates/user_item.html',
    'text!frontend/templates/image_item.html',
    'text!frontend/templates/gallery_item.html',
    'text!frontend/templates/memory_item.html',
    'text!frontend/templates/link_item.html',
    'text!frontend/templates/office_item.html',
    'text!frontend/templates/station_item.html',
    'text!frontend/templates/report_item.html',
    'text!frontend/templates/question_answer_item.html',
    'text!frontend/templates/market_item.html',
    'i18n!nls/labels',
    'map'
], function($, _, Backbone, BaseView, ItemModel, NewsTpl, EvaluaionTpl, EducationTpl, UserTpl, ImageTpl, GalleryTpl, MemoryTpl, LinkTpl, OfficeTpl, StationTpl, ReportTpl, QuestionAnswerTpl, MarketTpl, Labels) {

    var ItemView = BaseView.extend({
        events: {
            'click .delete': 'deleteItem',
            'click .edit': 'editItem',
            'click .switch-mode': 'switchViewMode',
            'click .edit-item': 'updateItem',
            'click .close-evaluation': 'showAlert',
            'click .btn-danger': 'closeEvaluation',
            'click .hide-alert': 'hideAlert',
//                'click :not(.icons)'        : 'showLogs'
        },
        initialize: function() {
            this.model.bind("change", this.render, this);
            switch (this.model.get("target")) {
                case "news":
                case "photoCoverage" :
                    this.template = _.template(NewsTpl);
                    break;

                case "questionsAnswers" :
                    this.template = _.template(QuestionAnswerTpl);
                    break;

                case "evaluations" :
                    this.template = _.template(EvaluaionTpl);
                    break;

                case "humans" :
                    this.template = _.template(UserTpl);
                    break;

                case "educations" :
                    this.template = _.template(EducationTpl);
                    break;

                case "galleries" :
                    this.template = _.template(GalleryTpl);
                    break;

                case "resources" :
                    this.template = _.template(ImageTpl);
                    break;

                case "memories" :
                    this.template = _.template(MemoryTpl);
                    break;

                case "links" :
                    this.template = _.template(LinkTpl);
                    break;

                case "offices" :
                    this.template = _.template(OfficeTpl);
                    break;

                case "stations" :
                    this.template = _.template(StationTpl);
                    break;

                case "reports" :
                    this.template = _.template(ReportTpl);
                    break;

                case "markets" :
                    this.template = _.template(MarketTpl);
                    break;
            }
        },
        render: function(fields) {
            this.$el.html(this.template(_.extend(this.model.toJSON(), {
                labels: Labels
            })));
            this.initMap()
            return this;
        },
        afterRender: function() {
            if (this.model.get("target") == "reports")
                this.initMap();

            this.$el.attr("id", this.model.get("id"));
            $(".hover-tooltip", this.$el).tooltip();
        },
        deleteAction: function() {
            var $this = this;
            bootbox.confirm(_.extend({
                'title': title || "",
                'message': "آیا برای حذف این مکان اطمینان دارید؟",
                'callback': function(result) {
                    $this.deletePlace();
                }
            }, options || {}));
        },
        deleteItem: function() {
            var $this = this;
            this.model.save({
                deleted: true
            }, {
                wait: true
            });
            this.dispose();
        },
        editItem: function() {
            Backbone.history.navigate(this.model.get("target") + "/" + this.model.get("id"), true);
        },
        switchViewMode: function() {
            var switch_mode = (this.model.get("viewMode") == "view") ? "edit" : "view";
            this.model.set("viewMode", switch_mode);
        },
        updateItem: function() {
            var data = _.extend($("form", this.$el).serializeJSON(), {
                viewMode: "view"
            });
            this.model.save(data, {
                success: function() {

                }
            })
        },
        initMap: function() {
            var location = [this.model.get('latitude'), this.model.get('longitude')];
            $(".gmap3", this.$el).gmap3({
                marker: {
                    latLng: location,
                    options: {
                        draggable: false
                    }
                },
                map: {
                    options: {
                        center: location,
                        zoom: 15,
                        mapTypeControl: true,
                        mapTypeControlOptions: {
                            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                        },
                        navigationControl: true,
                        scrollwheel: true,
                        streetViewControl: true,
                        zoomControl: true,
                        zoomControlOptions: {
                            position: google.maps.ControlPosition.RIGHT_TOP,
                            style: "SMALL"
                        },
                        panControl: false,
                        panControlOptions: {
                            position: google.maps.ControlPosition.RIGHT_TOP,
                            style: "SMALL"
                        }
                    }
                }
            });
        },
        showAlert: function() {
            $(".alert", this.$el).removeClass("hidden");
        },
        hideAlert: function() {
            $(".alert", this.$el).addClass("hidden");
        },
        closeEvaluation: function() {
            var $this = this;

            this.model.save({
                status: 1,
                action: "close"
            }, {
                success: function() {
                    $this.hideAlert();
                }
            })
        },
        showLogs: function() {
            Backbone.history.navigate(this.model.get("target") + '/' + this.model.get('id') + '/activities', true)
        }
    });

    return ItemView;
});

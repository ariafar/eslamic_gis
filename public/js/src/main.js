
require.config({
    baseUrl: "/js/src",
    waitSeconds: 120,
    paths: {
        'jquery': 'vendor/jquery/jquery',
        'jquery-ui': 'vendor/jquery-ui/js/jquery-ui-1.10.4.custom',
        'underscore': 'vendor/underscore/underscore',
        'underscore.string': 'vendor/underscore/underscore.string',
        'backbone': 'vendor/backbone/backbone',
        'bootstrap': 'vendor/bootstrap/js/bootstrap.min',
        'text': 'vendor/requirejs/requirejs-text',
        'json': 'vendor/requirejs/json',
        'i18n': 'vendor/requirejs/i18n',
        "select_picker": 'vendor/bootstrap-select/js/bootstrap-select',
        'icheck': 'vendor/iCheck-1.x/icheck.min',
        'pnotify': 'vendor/pnotify/pnotify.custom.min',
        'bootbox': 'vendor/bootbox/bootbox',
    },
    locale: 'fa-ir',
    shim: {
        'bootstrap': {
            deps: ['jquery']
        },
        'underscore.string': {
            deps: ['underscore']
        },
        'select_picker': {
            deps: ['bootstrap']
        },
        'jquery-ui': {
            deps: ['jquery']
        },
        'icheck': {
            deps: ['jquery']
        },
        'bootbox': {
            deps: ['jquery', 'bootstrap']
        }
    }
});

require(['jquery', 'jquery-ui', 'underscore', 'backbone', 'app', 'router', 'bootstrap'], function($, jUI, _, Backbone, App, Router) {

    App.initialize();
    App.ajaxSetup();

    __ = App.Lang.format;

    Events = _.extend({}, Backbone.Events);

    var router = new Router();
    Backbone.history.start({
        //        root: '/'
    });

    (function($) {
        $.fn.serializeJSON = function() {
            var json = {};
            jQuery.map($(this).serializeArray(), function(n, i) {
                json[n['name']] = n['value'];
            });

            $(this).find('input[type="checkbox"]').each(function() {
                if ($(this).attr('name') && typeof json[$(this).attr('name')] != 'undefined')
                    json[$(this).attr('name')] = true;
            });

            return json;
        };
    })(jQuery);

    $(window).bind("load resize", function() {
        topOffset = 50;
        width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse')
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse')
        }

        height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
        height = height - topOffset;
        if (height < 1)
            height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    })



});

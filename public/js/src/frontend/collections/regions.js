define([
    'backbone',
    'frontend/collections/base',
    'frontend/models/region',
    ], function(Backbone, BaseCollection, RegionModel) {

        var RegionsCollection = BaseCollection.extend({
            
            model : RegionModel,

            url: function() {
                return 'api/regions'
            }

        
        });

        return RegionsCollection;
    });

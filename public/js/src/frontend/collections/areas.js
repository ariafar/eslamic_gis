define([
    'backbone',
    'frontend/collections/base',
    'frontend/models/area',
    ], function(Backbone, BaseCollection, AreaModel) {

        var AreasCollection = BaseCollection.extend({
            
            model : AreaModel,
            
            url: function() {
                return 'api/regions/' + this.options.regionid + '/areas'
            }

        
        });

        return AreasCollection;
    });

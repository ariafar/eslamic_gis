define([
    'backbone',
    'frontend/collections/base',
    'frontend/models/user',
    ], function(Backbone, BaseCollection, UserModel) {

        var AreasCollection = BaseCollection.extend({
            
            model : UserModel,
            
            url: function() {
                return 'api/users/';
            }

        
        });

        return AreasCollection;
    });

define([
    'app',
    'underscore',
    'backbone',
    'frontend/views/base_list',
    'text!frontend/templates/place/list.html',
    'text!frontend/templates/place/item.html',
    'text!frontend/templates/place/publish.html',
    'frontend/collections/places',
    'frontend/views/place_form',
    'i18n!nls/labels',
], function(App, _, Backbone, BaseListCmp, ListTpl, ItemTpl, PublishTpl, PlacesCollection, PlaceFomrView, Labels) {

    var RegionsListView = BaseListCmp.extend({
        fetchCollection: true,
        initialize: function(options) {

            this.events = _.extend({
                'keydown input': 'preventDefualt',
                'click .add-place': 'showForm',
                'click .second-navbar .tab': 'changeTap',
                'click .published': 'publishePlace',
                'click .rejected': 'rejectPlace',
                'click .publish': 'publish',
                'click .delete-place': 'deleteAction',
                'keyup .search': 'searchByName',
                'click .input-group-addon': 'resetSerahcByName',
                'click .edit': 'editPlace'
            }, this.events);

            this.template = _.template(ListTpl);
            this.itemTpl = _.template(ItemTpl);

            this.collection = new PlacesCollection([]);

            RegionsListView.__super__.initialize.call(this, options);
        },
        addRow: function(model, mode) {
            var mode = mode || "create",
                    item_el = this.itemTpl(_.extend(model.toJSON(), {
                        labels: Labels,
                        type: $(".second-navbar .selected").data("type")
                    }));

            mode == "create" ?
                    $(".list-holder", this.$el).append(item_el) :
                    $(".item-wrapper#" + model.get("id"), this.$el).replaceWith(item_el);
        },
        preventDefualt: function(e) {
            if (e.keyCode == 13)
                return false;
        },
        addNewItem: function() {
            var obj = $("form", this.$el).serializeJSON(),
                    new_item = new this.collection.model(),
                    $this = this;

            new_item.save(obj, {
                success: function(model) {
                    $this.collection.add(model);
                }
            })
        },
        showRegions: function() {
            this.areasView.dispose();
            this.render();
            this.addRows();
        },
        showAreas: function(e) {
            this.areasView && this.areasView.dispose();

            var regionId = $(e.target).closest(".row").data("region"),
                    region = this.collection.get(regionId);

            if (region) {
                this.areasView = new AreasView({
                    region: region
                });

                this.$el.html(this.areasView.$el);
            }
        },
        dispose: function() {
            this.areasView && this.areasView.dispose();
            RegionsListView.__super__.dispose.call(this);
        },
        deleteRegion: function(e) {
            var el = $(e.target).closest(".row"),
                    regionId = el.data("region"),
                    region = this.collection.get(regionId),
                    $this = this;

            region.destroy({
                wait: true
            });

            el.remove();
        },
        changeTap: function(e) {

            var el = $(e.target),
                    type = el.data("type");

            if (!el.hasClass("selected")) {
                $(".second-navbar .selected", this.$el).removeClass("selected");
                el.addClass("selected");
            }

            this.reset();

            if (type == "owne-place") {
                this.fetch({
                    "createBy": window.currentUser.get("id")
                });
            }
            else if (type == "wait-place") {
                this.fetch({
                    "status": "1"
                });
            }
        },
        reset: function() {

            $(".list-holder", this.$el).empty();

            this.collection.filters.reset();
        },
        fetch: function(filters) {
            var filters = filters || {
                "createBy": window.currentUser.get("id")
            }

            this.collection.reset();
            this.collection.filters.set(filters, {
                silent: true
            });

            this.collection.fetch();
        },
        rejectPlace: function(e) {
            var el = $(e.target).closest(".item-wrapper"),
                    id = el.attr("id"),
                    place = this.collection.get(id);

            place.save({
                status: 3
            }, {
                success: function() {
                    el.remove();
                }
            })
        },
        publishePlace: function(e) {
            var el = $(e.target).closest(".item-wrapper"),
                    id = el.attr("id"),
                    place = this.collection.get(id);

            var tpl = _.template(PublishTpl);
            $(".modal", this.$el).html(tpl({
                id: id,
                labels: Labels
            }))

            $(".modal", this.$el).modal("show");

        },
        publish: function(e) {
            var id = $(e.target).data("id"),
                    place = this.collection.get(id),
                    $this = this;

            place.save({
                status: "2",
                isPrivate: $(".modal [name=isPrivate]", this.$el).val()
            }, {
                success: function() {
                    $(".item-wrapper#" + id).remove();
                    $(".modal", $this.$el).modal("hide");
                }
            })
        },
        deleteItem: function(e) {
            var el = $(e.target).closest(".row"),
                    placeId = el.attr("id"),
                    place = this.collection.get(placeId);

            place.destroy({
                success: function(model, resp) {
                    el.remove();
                },
                error: function(model, error) {
                    App.Notify('error', Labels.forbidden_delete, '', {
                        addclass: 'stack-bottomleft custom',
                        stack: "stack_bottomleft",
                        icon: 'picon picon-32 picon-fill-color',
                        opacity: .8,
                        nonblock: {
                            nonblock: true
                        }
                    });
                }
            });
        },
        editPlace: function(e) {

            var el = $(e.target).closest(".row"),
                    placeId = el.attr("id"),
                    place = this.collection.get(placeId),
                    $this = this;

            if (!place)
                return;

            $(".list-wrapper", this.$el).hide();
            $(".form-wrapper", this.$el).show();

            this.formView && this.formView.dispose();
            place.set("status", 1);
            this.formView = new PlaceFomrView({
                model: place,
                onSaveSuccess: function(obj) {
                    place.set(obj);
                    $(".form-wrapper", $this.$el).hide();
                    $(".list-wrapper", $this.$el).show();
                    $this.addRow(place, 'update');
                }
            });

            $(".form-wrapper", this.$el).html(this.formView.$el);
            this.formView.afterRender && this.formView.afterRender();
        }

    });
    return RegionsListView
});
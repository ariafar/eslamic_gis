
define([
    'app',
    'jquery',
    'underscore',
    'backbone',
    'frontend/views/form',
    'frontend/collections/regions',
    'frontend/collections/place_types',
    'frontend/collections/users',
    'text!frontend/templates/user/form.html',
    'text!frontend/templates/user/placeTypes_selection.html',
    'frontend/views/regions_selection',
    'frontend/models/user',
    'i18n!nls/labels',
    'select_picker',
    'icheck',

    ], function (App, $, _, Backbone, FormView, RegionsCollection, PlaceTypesCollection, UsersCollection, Tpl, PlaceTypeSelctionTpl, RegionsSelectionView, UserModel, Labels) {

        var ItemView = FormView.extend({
            
            className : "user-form",
            
            initialize : function(options) {

                this.events = _.extend({
                    //                    'click #display-countries'      : 'popCountrySelection',
                    //                    'click .select-countries'       : 'selectCountries',
                    'click .remove-country'         : 'removeSelectedCountry',

                    //                    'click #display-placeTypes'     : 'popPlaceTypesSelection',
                    'click .select-placeTyes'       : 'selectPlaceTypes',
                    'click .remove-placeType'       : 'removeSelectedPlaceType',

                }, this.events);

                this.template = _.template(Tpl);
                this.options = options || {};
                this.selectedCountries = [];
                this.selectedPlaceTypes = [];
                
                if(!this.model)
                    this.model = new UserModel();
                else{
                    this.model.fetch({
                        async : false
                    })
                }
                
                this.regionsCollection = new RegionsCollection([]);
                this.regionsCollection.fetch({
                    async : false
                });

                this.placeTypesCollection = new PlaceTypesCollection([]);
                this.placeTypesCollection.fetch({
                    async : false
                });

                this.usersCollection = new UsersCollection([]);
                this.render();
                
                return this;
            },

            render : function() {
                this.$el.html(this.template(_.extend(this.model.toJSON(), {
                    labels      : Labels,
                    countries   : this.regionsCollection.toJSON(),
                    regions     : this.regionsCollection.toJSON(),
                    placeTypes  : this.placeTypesCollection.toJSON()
                })));
            },

            afterRender : function(){
                var $this = this;

                this.getSustemUsers();

                $('input.iCheck', this.$el).iCheck({
                    checkboxClass: 'icheckbox_flat-purple',
                    radioClass: 'iradio_flat-purple'
                });

                this.registerEvents();
            },

            reset : function(){
                this.model = new UserModel();
                this.render();
                this.afterRender();
                this.selectedCountries = [];
                this.selectedPlaceTypes = [];
            },

            registerEvents : function(){
                var $this = this;
                
                $('input.write-only', this.$el).on('ifChanged', function(){
                    if($(this).is(":checked"))
                        $(".select-header").show();
                    else
                        $(".select-header").hide();
                });


                $("input[type=checkbox]").iCheck('disable');

                $('input.region-checkbox', this.$el).on('ifClicked', function(){
                    $this.onClickRegion($(this));
                });

                $('input.area-checkbox', this.$el).on('ifClicked', function(){
                    $this.onClickArea($(this));
                });

                $('input.country-checkbox', this.$el).on('ifToggled', function(){
                    $this.onToggleCountry($(this));
                });

                $('input.area-checkbox', this.$el).on('ifToggled', function(){
                    $this.onToggleArea($(this));
                });

                $('input.place-type', this.$el).on('ifToggled', function(){
                    $this.onSelectPlaceType($(this));
                });
                
                $('input[name=permission]', this.$el).on('ifToggled', function(){
                    $this.onToggleUserPermission($(this));
                });


                $('.collapse').collapse();

                if(this.model.get("placeTypes") && this.model.get("placeTypes").length){
                    $(".toggle-show-settings[value=3]").iCheck("check");
                    _.each(this.model.get("placeTypes"), function(obj){
                        if(obj.access == 0)
                            $(".place-type.public[data-id="+ obj.placeTypeId +"]").iCheck("check");
                        else
                            $(".place-type.private[data-id="+ obj.placeTypeId +"]").iCheck("check");
                    });

                }
                else if(this.model.get("countries") && this.model.get("countries").length){
                    $(".toggle-show-settings[value=2]").iCheck("check");
                    _.each(this.model.get("countries"), function(obj){
                        if(obj.write == 2)
                            $(".country-checkbox.write_only[data-id="+ obj.countryId +"]").iCheck("check");
                        else
                            $(".country-checkbox.write_full[data-id="+ obj.countryId +"]").iCheck("check");
                    });
                    
                }
               
            },

            onClickRegion : function(el){
                
                var region_id = el.data("region"),
                type = $(el).data("type"),
                holder = $("#collapse-region-" + region_id);
                
                if(!el.is(":checked"))
                    $("input[data-type="+ type +"]" , holder).iCheck('check');
                else
                    $("input[data-type="+ type +"]" , holder).iCheck('uncheck');
            },

            onClickArea : function(el){
                var area_id = el.data("area"),
                type = $(el).data("type"),
                holder = $("#collapse-area-" + area_id);
                
                if(!el.is(":checked")){
                    $("input[data-type="+ type +"]" , holder).iCheck('check');
                }
                else{
                    $("input[data-type="+ type +"]" , holder).is(":checked") && $("input[data-type="+ type +"]", holder).iCheck('uncheck');
                }
            },

            onToggleArea : function(el){
                
                var parent = $(el.closest(".panel-group")),
                type = $(el).data("type"),
                region_id = el.data("region");

                if( $(".area-checkbox:checked[data-type="+ type +"]", parent).length == $(".area-checkbox[data-type="+ type +"]", parent).length ){
                    $("input.region-" + region_id+ "[data-type="+ type+"]" ).iCheck('check');
                }else{
                    $("input.region-" + region_id + "[data-type="+ type+"]" ).is(":checked") && $("input.region-" + region_id + "[data-type="+ type+"]" ).iCheck('uncheck');
                }
            },

            onToggleCountry : function(el){
                var area_id = el.data("area"),
                type = $(el).data("type"),
                id = $(el).data("id"),
                parent = $(el.closest(".panel-body"));

                if(el.is(":checked")){
                    if( $(".country-checkbox[data-id="+ id +"]", parent).length == 2 ){
                        if(type == "write_full")
                            $("#country-"+ id +"[data-type=write_only]", parent).iCheck('uncheck');
                        else
                            $("#country-"+ id +"[data-type=write_full]", parent).iCheck('uncheck');
                    }
                }


                if(el.is(":checked")){
                    if( $(".country-checkbox:checked[data-type="+ type +"]", parent).length == $(".country-checkbox[data-type="+ type +"]", parent).length )
                        $("input.area-" + area_id + "[data-type="+ type+"]" ).iCheck('check');
                }else{
                    $("input.area-" + area_id + "[data-type="+ type+"]" ).is(":checked") && $("input.area-" + area_id + "[data-type="+ type +"]").iCheck('uncheck');
                }

               


            },

            toggleShowSystemUserSettings : function(el){
                
                var  settings_el = $("#" + el.data("toggle") + "-settings");
                
                if (!el.is(":checked")) {
                    settings_el.animate({
                        height : 200
                    }, {
                        duration: 200,
                        done: function() {
                            el.css('overflow', 'visible');
                        }
                    });

                } else {

                    settings_el.animate({
                        height : 0
                    }, {
                        duration: 200,
                        done: function() {
                            el.css('overflow', 'hidden');
                        }

                    });
                    $('i', el).removeClass('aticon-arrow-right3');
                    $('i', el).addClass('aticon-arrow-left3');

                    $(el).data('status', 'open');
                    $(el).addClass('toolbar-selected');
                }

            },

            onSelectPlaceType : function(el){
                var parent = el.closest(".user-placeTypes-selection"),
                type = el.data("type");
                
                if(el.is(":checked")){
                    if( $(".place-type:checked", parent).length == $(".place-type", parent).length ){
                        if(type == "public")
                            $("input.private", parent).iCheck('uncheck');
                        else
                            $("input.public", parent).iCheck('uncheck');
                    }
                }
            },

            popCountrySelection : function(e){

                if($(e.target).closest("li").length)
                    return;
                
                var $this = this;
                
                this.regionsSelectionView =  new RegionsSelectionView({
                    collection           : this.regionsCollection
                });

                $(".modal-body", this.$el).html( this.regionsSelectionView.$el);

                $(".modal", this.$el).modal("show");
                

                $(".modal", this.$el).on('shown.bs.modal', function (e) {

                    $this.regionsSelectionView.registerEvents();

                    if($this.selectedCountries.length){
                        _.each($this.selectedCountries, function(id){
                            $('.modal input#country-' + id , $this.$el).iCheck('check');
                        })
                    }
                });
            },

            onToggleUserPermission : function(){
                if($("[name=permission]:checked").val() == 2){
                    $("#readPermission-settings input.iCheck").iCheck('uncheck');
                    $("#readPermission-settings input.iCheck").iCheck('disable');
                    $("#writePermission-settings input.iCheck").iCheck('enable');
                }
                else{
                    $("#writePermission-settings input.iCheck").iCheck('uncheck');
                    $("#writePermission-settings input.iCheck").iCheck('disable');
                    $("#readPermission-settings input.iCheck").iCheck('enable');
                }
            },

            //            selectCountries : function(){
            //                var $this = this;
            //                this.selectedCountries = [];
            //                $("#display-countries").empty();
            //
            //                $(".country-checkbox:checked").each(function(index, el){
            //                    $this.showSelectedCountries({
            //                        id      : $(el).data("id"),
            //                        name    : $(el).data("name")
            //                    });
            //
            //                    $this.selectedCountries.push($(el).data("id"));
            //                });
            //
            //                $(".modal", this.$el).modal("hide");
            //            },

            showSelectedCountries : function(data){
                var el = $('<li class="selected-country" id="'+ data.id +'"><span>'+ data.name +'</span><i class="fa fa-times remove-country"></i></li>')
                $("#display-countries").append(el);
            },

            removeSelectedCountry : function(e){
                var el = $(e.target).closest(".selected-country"),
                country_id = el.attr("id");
                el.remove();
                
                this.selectedCountries = $.grep(this.selectedCountries, function(id){
                    return (id != country_id);
                });

                return false;
            },

            popPlaceTypesSelection : function(e){
                if($(e.target).closest("li").length)
                    return;

                var $this = this,
                tpl = _.template(PlaceTypeSelctionTpl);

                $(".modal", this.$el).html(tpl({
                    placeTypes        : this.placeTypesCollection.toJSON(),
                    labels            : Labels
                }));

                $(".modal", this.$el).modal("show");

                $(".modal", this.$el).on('shown.bs.modal', function (e) {
                    $('.modal input', $this.$el).iCheck({
                        checkboxClass: 'icheckbox_flat-purple',
                        radioClass: 'iradio_flat-purple'
                    });

                    $(".modal input").css("visibility", "visible");

                    if($this.selectedPlaceTypes.length){
                        _.each($this.selectedPlaceTypes, function(id){
                            $('.modal input#' + id , $this.$el).iCheck('check');
                        })
                    }
                });
            },

            selectPlaceTypes  : function(){
                var $this = this;
                this.selectedPlaceTypes = [];
                $("#display-placeTypes").empty();

                $(".placeType:checked").each(function(index, el){
                    $this.showSelectedPlaceTypes({
                        id      : $(el).attr("id"),
                        name    : $(el).data("name")
                    });

                    $this.selectedPlaceTypes.push($(el).attr("id"));
                });

                $(".modal", this.$el).modal("hide");
            },

            showSelectedPlaceTypes : function(data){
                var el = $('<li class="selected-placeType" id="'+ data.id +'"><span>'+ data.name +'</span><i class="fa fa-times remove-placeType"></i></li>')
                $("#display-placeTypes").append(el);
            },

            removeSelectedPlaceType : function(e){
                var el = $(e.target).closest(".selected-placeType"),
                country_id = el.attr("id");
                el.remove();

                this.selectedPlaceTypes = $.grep(this.selectedPlaceTypes, function(id){
                    return (id != country_id);
                });

                return false;
            },

            getSustemUsers : function(){
                var $this = this;
                if(!this.usersCollection.isFetched){
                    this.usersCollection.filters.set({
                        "type"       : 2,
                        "permission" : 2
                    },{
                        silent : true
                    });
                    
                    this.usersCollection.fetch({
                        async : false,
                        success : function(){
                            $this.usersCollection.isFetched = true;
                            $this.showUsers();
                        }
                    });
                }
           
            },

            showUsers : function(){
                var cnt = $("[name=headeId]");
                this.usersCollection.each(function(user){
                    var el = $("<option value='" + user.get("id") + "'>"+ user.get("firstName") + ' ' + user.get("lastName")+"</option>")
                    cnt.append(el);
                });
            },

            setImage : function(e){
                var input = $('#file')[0];

                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('.user-form .user-image').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            },
            openSelectionFile : function(){
                $("#file").trigger("click");
            },


            validate : function(){
                var validate =  true;
                var data = $("form", this.$el).serializeJSON();

                $("form [required]").each(function(index, el){
                    if($(el).val() == ""){
                        validate =  false;
                        $(el).parent().addClass("error");

                    }
                });
                if(!validate){
                    App.showError(Labels.fill_required_fields);
                    return;
                }


                if($("[name=permission]:checked").length == 0){
                    validate = false;
                    App.showError(Labels.set_user_type);
                    return;
                }

                if($("[name=permission]:checked").val() == 2 && $(".country-checkbox:checked").length == 0){
                    validate =  false;
                    $('#display-countries', this.$el).parent().addClass("error");
                    App.showError(Labels.user_need_settings);
                }

                if($("[name=permissino]:checked").val() == 3 && $(".place-type:checked").length == 0){
                    validate =  false;
                    $('#display-placeTypes', this.$el).parent().addClass("error");
                    App.showError(Labels.user_need_settings);
                }

                if($(".country-checkbox[data-type=write_only]:checked").length && $("[name=headeId]").val() == "" ){
                    validate =  false;
                    $('.select-header', this.$el).addClass("error");
                    App.showError(Labels.user_need_settings);
                }



                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if(data.email != "" && !re.test(data.email)){
                    $("[name=email]").parent().addClass("error");
                    validate = false;
                }

                var re = /^\+?\d+(-\d+)*$/;
                if(data.tell != "" && !re.test(data.tell)){
                    $("[name=tell]").parent().addClass("error");
                    validate = false;
                }

                if(!this.model.get("id")){
                    if(data.password && data.password.length < 5){
                        $("[name=password]").parent().addClass("error");
                        App.showError(Labels.morthan6charachterwarnin);
                        validate = false;
                        return;
                    }else if(data.password != data.confirm){
                        $("[name=confirm]").parent().addClass("error");
                        App.showError(Labels.mathpasswordwarning);
                        validate = false;
                        return;
                    }
                }


                if(!validate){
                    App.showError(Labels.correct_invalid_values);
                    $(".rquired_error").css("visibility", "visible");
                }

                return validate;
            },

            setUserCountries: function(){

                this.writeOnlyCountries = [];
                this.wirteFullCountries = [];

                var $this = this;
                
                $(".country-checkbox:checked").each(function(index, el){
                    var id = $(el).data("id");
                    var type = $(el).data("type");
                    if(type == "write_only"){
                        $this.writeOnlyCountries.push(id);
                    }else{
                        $this.wirteFullCountries.push(id);
                    }
                });
            },

            setUserPlaceTypes : function(){
                this.publicPlaces = [];
                this.privatePlaces = [];

                var $this = this;
                $(".place-type:checked").each(function(index, el){
                    var id = $(el).data("id");
                    var type = $(el).data("type");
                    if(type == "public"){
                        $this.publicPlaces.push(id);
                    }else{
                        $this.privatePlaces.push(id);
                    }
                });
            },

            submit : function(){
                
                $(".error").removeClass("error");
                $(".rquired_error").css("visibility", "hidden");
                $(".error_msg").css("visibility", "hidden");
                
                if(!this.validate())
                    return;


                this.setUserCountries();
                this.setUserPlaceTypes();

                $(".modal-footer .btn:not(.btn-default)").attr("disabled", true);
                
                var data =  _.extend(this.model.toJSON(), $("form", this.$el).serializeJSON()),
                $this = this,
                mode = this.model.get("id") ? "edit" : "create";
                
                if(!data.isSystemUser){
                    delete data.writePermission
                }
            

                var fd = new FormData();
                fd.append( 'file', $('#file')[0].files[0] );
                
                for(key in data){
                    if (data[key])
                        fd.append(key, data[key]);
                }

                if($("[name=permission]:checked").val() == "2"){
                    if( this.writeOnlyCountries.length ){
                        fd.append('writeOnlyCountries', this.writeOnlyCountries.join(','))
                    }

                    if( this.wirteFullCountries.length ){
                        fd.append('wirteFullCountries', this.wirteFullCountries.join(','))
                    }
                }
                else{
                    
                    if( this.publicPlaces.length ){
                        fd.append('publicPlaces', this.publicPlaces.join(','))
                    }

                    if( this.privatePlaces.length ){
                        fd.append('privatePlaces', this.privatePlaces.join(','))
                    }
                }

                
                $.ajax({
                    type: "POST",
                    url: "/api/users/",
                    processData: false,
                    contentType: false,
                    data: fd,
                    dataType : "JSON",
                    success: function (obj) {
                        if($this.options.onSaveSuccess)
                            $this.options.onSaveSuccess(obj);
                        else{
                            App.Notify('success', Labels.save_success, '', {
                                addclass: 'stack-bottomleft custom',
                                stack: "stack_bottomleft",
                                icon: 'picon picon-32 picon-fill-color',
                                opacity: .8,
                                nonblock: {
                                    nonblock: true
                                }
                            });
                            $this.reset();
                        }
                    },
                    error : function(error){
                        App.Notify('error', error.responseText, '', {
                            addclass: 'stack-bottomleft custom',
                            stack: "stack_bottomleft",
                            icon: 'picon picon-32 picon-fill-color',
                            opacity: .8,
                            nonblock: {
                                nonblock: true
                            }
                        });
                    }
                });

                return false;
            },

            changeIcon : function(e){
                var el = $(e.target).closest(".panel-title");
                $("i", el).toggleClass("glyphicon-plus-sign").toggleClass("glyphicon-minus-sign");
            }

        });

        return ItemView;
    });

define([
    'app',
    'underscore',
    'backbone',
    'frontend/views/base_list',
    'text!frontend/templates/region/list.html',
    'text!frontend/templates/region/item.html',
    'text!frontend/templates/region/form.html',
    'frontend/collections/regions',
    'frontend/views/areas',
    'frontend/collections/areas',
    'i18n!nls/labels',
], function(App, _, Backbone, BaseListCmp, ListTpl, ItemTpl, FormTpl, RegionsCollection, AreasView, AreasCollection, Labels) {

    var RegionsListView = BaseListCmp.extend({
        events: {
            'keydown input': 'preventDefualt',
            'click .add-region': 'addNewItem',
            'click .show-areas': 'showAreas',
            'click .show-regions': 'showRegions',
            'click .delete-region': 'deleteAction',
            'click .edit-region': 'showForm',
            'click .update-region': 'update'
        },
        fetchCollection: true,
        initialize: function(options) {
            this.template = _.template(ListTpl);
            this.itemTpl = _.template(ItemTpl);

            this.collection = new RegionsCollection([]);
            RegionsListView.__super__.initialize.call(this, options);
        },
        preventDefualt: function(e) {
            if (e.keyCode == 13)
                return false;
        },
        addNewItem: function() {
            var obj = $("form", this.$el).serializeJSON(),
                    new_item = new this.collection.model(),
                    $this = this;

            new_item.save(obj, {
                success: function(model) {
                    $("input[name=name]").val("");
                    $this.collection.add(model);
                }
            })
        },
        showRegions: function() {
            this.areasView.dispose();
            this.render();
            this.addRows();
        },
        showAreas: function(e) {
            this.areasView && this.areasView.dispose();
            var regionId = $(e.target).closest(".row").data("region"),
                    region = this.collection.get(regionId);

            if (region) {
                if (!region.areasCollection) {
                    region.areasCollection = new AreasCollection(region.get("areas") || [], {
                        regionId: region.get("id")
                    });
                }

                this.areasView = new AreasView({
                    region: region,
                    collection: region.areasCollection
                });

                this.$el.html(this.areasView.$el);
            }
        },
        dispose: function() {
            this.areasView && this.areasView.dispose();
            RegionsListView.__super__.dispose.call(this);
        },
        deleteItem: function(e) {
            var el = $(e.target).closest(".row"),
                    regionId = el.data("region"),
                    region = this.collection.get(regionId);

            region.destroy({
                wait: true,
                success: function(model, resp) {
                    el.remove();
                },
                error: function(model, error) {
                    App.Notify('error', Labels.forbidden_delete, '', {
                        addclass: 'stack-bottomleft custom',
                        stack: "stack_bottomleft",
                        icon: 'picon picon-32 picon-fill-color',
                        opacity: .8,
                        nonblock: {
                            nonblock: true
                        }
                    });
                }
            });

        },
        showForm: function(e) {
            var el = $(e.target).closest(".row"),
                    id = el ? el.data("region") : null,
                    model = this.collection.get(id);

            model && this.popForm(model);
        },
        popForm: function(model) {
            var $this = this;

            var tpl = _.template(FormTpl);

            $("#popup-holder", this.$el).html(tpl(_.extend(model.toJSON(), {
                labels: Labels
            })));

            $(".modal", this.$el).modal("show");

            $("#popup-holder", this.$el).modal({
                backdrop: "static"
            });
        },
        update: function(e) {
            var id = $(e.target).data("id"),
                    model = this.collection.get(id),
                    $this = this;

            if (model.get("name") != $(".modal [name=name]", this.$el).val()) {
                model.save({
                    name: $(".modal [name=name]", this.$el).val()
                }, {
                    success: function() {
                        $(".modal", $this.$el).modal("hide");
                        var holder = $("[data-region=" + model.get("id") + "]");
                        $("span.name", holder).html(model.get("name"))
                    }
                })
            }
        }


    });
    return RegionsListView
});
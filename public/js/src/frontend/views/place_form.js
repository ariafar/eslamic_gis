
define([
    'app',
    'underscore',
    'backbone',
    'frontend/views/form',
    'frontend/collections/countries',
    'frontend/collections/place_types',
    'text!frontend/templates/place/form.html',
    'text!frontend/templates/place/map.html',
    'frontend/models/place',
    'i18n!nls/labels',
    'select_picker',
    'pnotify'

    ], function (App, _, Backbone, FormView, CountriesCollection, PlaceTypesCollection, Tpl, MapTpl, PlaceModel, Labels) {

        var ItemView = FormView.extend({
            
            className : "place-form",
            
            initialize : function(options) {

                this.events = _.extend({
                    'click .pop-map'        : 'popMap',
                    'click .save-position'  : 'setPosition'
                    
                }, this.events)
                this.options = options || {};
                this.template = _.template(Tpl);

                if(!this.model)
                    this.model = new PlaceModel();
                
                this.countriesCollection = new CountriesCollection([],{
                    url : 'api/users/' + currentUser.get("id") + "/countries"
                });
                this.countriesCollection.fetch({
                    async : false
                });

                this.placeTypesCollection = new PlaceTypesCollection([]);
                this.placeTypesCollection.fetch({
                    async : false
                });

                this.render();
            },

            render : function() {
                this.$el.html(this.template(_.extend(this.model.toJSON(), {
                    labels      : Labels,
                    countries   : this.countriesCollection.toJSON(),
                    placeTypes  : this.placeTypesCollection.toJSON()
                })));
                
                return this;
            },

            afterRender : function(){
                
                $("select", this.$el).selectpicker({
                    width :  "450px"
                });
            },

            reset : function(){
                this.model = new PlaceModel();
                this.render();
            },

            popMap : function(){
                
                var $this = this,
                tpl = _.template(MapTpl);

                $(".modal", this.$el).html(tpl({
                    placeTypes        : this.placeTypesCollection.toJSON(),
                    labels            : Labels
                }));

                $(".modal", this.$el).modal("show");
                
                $(".modal", this.$el).on('shown.bs.modal', function (e) {
                    $this.loadMap();
                });
            },

            loadMap : function(){

                var lat = ( $("[name=latitude]").val() || 35.744 ),
                lng = ( $("[name=longitude]").val() || 51.3750);

                var mapOptions = {
                    zoom: 2,
                    center: new google.maps.LatLng(lat, lng),
                    mapTypeControl: true,
                    mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                        position: google.maps.ControlPosition.BOTTOM_CENTER
                    },
                    panControl: true,
                    panControlOptions: {
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    zoomControl: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.LARGE,
                        position: google.maps.ControlPosition.LEFT_CENTER
                    },
                    scaleControl: true,  // fixed to BOTTOM_RIGHT
                    streetViewControl: true,
                    streetViewControlOptions: {
                        position: google.maps.ControlPosition.LEFT_TOP
                    }

                };

                this.map = new google.maps.Map(document.getElementById('map-canvas'),
                    mapOptions);

                this.marker = new google.maps.Marker({
                    map: this.map,
                    draggable:true,
                    animation: google.maps.Animation.DROP,
                    position: new google.maps.LatLng(lat, lng)
                });

            },

            setPosition : function(){
                $("[name=latitude]").val(this.marker.getPosition().lat());
                $("[name=longitude]").val(this.marker.getPosition().lng());
                $(".modal", this.$el).modal("hide");
            },

            validate : function(){

                var validate =  true;
                var data = $("form", this.$el).serializeJSON();

                $("form [required]").each(function(index, el){
                    if($(el).val() == ""){
                        validate =  false;
                        $(el).parent().addClass("error");
                    }
                });

                if(!validate){
                    App.showError(Labels.fill_required_fields);
                    return;
                }

                var latlngVal = /^-?([0-8]?[0-9]|90)\.[0-9]{1,6},-?((1?[0-7]?|[0-9]?)[0-9]|180)\.[0-9]{1,6}$/,
                latReg = /^-?([0-8]?[0-9]|90)\.[0-9]{1,20}$/,
                longReg = /^-?((1?[0-7]?|[0-9]?)[0-9]|180)\.[0-9]{1,20}$/,
                latitude  =  $("[name=latitude]").val(),
                longitude = $("[name=longitude]").val(),
                latlng = latitude + ',' + longitude;

                if(!latitude || latitude == "" || !latReg.test(latitude)) {
                    $("[name=latitude]").addClass("error");
                    validate = false;
                }
                
                if(!longitude || longitude == "" || !longReg.test(longitude)) {
                    $("[name=longitude]").addClass("error");
                    validate = false;
                }

                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if(data.email != "" && !re.test(data.email)){
                    $("[name=email]").parent().addClass("error");
                    validate = false;
                }

                var re = /((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)/i;
                if(data.website != "" && !re.test(data.website)){
                    $("[name=website]").parent().addClass("error");
                    validate = false;
                }

                var re = /^\+?\d+(-\d+)*$/;
                if(data.tell != "" && !re.test(data.tell)){
                    $("[name=tell]").parent().addClass("error");
                    validate = false;
                }

                if(!validate){
                    App.showError(Labels.correct_invalid_values);
                    $(".rquired_error").css("visibility", "visible");
                }

                return validate;
            },

            submit : function(){
                
                $(".error").removeClass("error");
                $(".rquired_error").css("visibility", "hidden");
                $(".error_msg").css("visibility", "hidden");
                
                if(!this.validate())
                    return;
                
                $(".modal-footer .btn:not(.btn-default)").attr("disabled", true);
                
                var data =  _.extend(this.model.toJSON(), $("form", this.$el).serializeJSON()),
                $this = this,
                mode = this.model.get("id") ? "edit" : "create";

                var fd = new FormData();
                fd.append( 'file', $('#file')[0].files[0] );
                for(key in data){
                    if (data[key])
                        fd.append(key, data[key]);
                }
                
                $.ajax({
                    type: "POST",
                    url: "/api/places" ,
                    processData: false,
                    contentType: false,
                    data: fd,
                    dataType : "JSON",
                    success: function (obj) {
                        if($this.options.onSaveSuccess)
                            $this.options.onSaveSuccess(obj);
                        else{
                            App.Notify('success', Labels.save_success, '', {
                                addclass: 'stack-bottomleft custom',
                                stack: "stack_bottomleft",
                                icon: 'picon picon-32 picon-fill-color',
                                opacity: .8,
                                nonblock: {
                                    nonblock: true
                                }
                            });
                            $this.reset();
                        }
                    },
                    error : function(error){
                        console.log(error);
                    }
                });

                return false;
            }
            
        });

        return ItemView;
    });

define([
    'app',
    'underscore',
    'backbone',
    'frontend/views/base_list',
    'text!frontend/templates/user/list.html',
    'text!frontend/templates/user/item.html',
    'frontend/collections/users',
    'frontend/views/user_form',
    'i18n!nls/labels',
], function(App, _, Backbone, BaseListCmp, ListTpl, ItemTpl, UsersCollection, UserFomrView, Labels) {

    var RegionsListView = BaseListCmp.extend({
        fetchCollection: true,
        initialize: function(options) {

            this.events = _.extend(this.events, {
                'keydown input': 'preventDefualt',
                'click .delete-user': 'deleteAction',
                'keyup .search': 'searchByName',
                'click .input-group-addon': 'resetSerahcByName',
                'click .edit': 'editUser',
                'click .reset-password': 'resetPassword'
            });


            this.template = _.template(ListTpl);
            this.itemTpl = _.template(ItemTpl);

            this.collection = new UsersCollection([]);
            RegionsListView.__super__.initialize.call(this, options);
        },
        deleteItem: function(e) {
            var el = $(e.target).closest(".row"),
                    id = el.attr("id"),
                    user = this.collection.get(id);
            if(!user){
                return;
            }
            user.destroy({
                wait : true,
                success: function(model, resp) {
                    el.remove();
                },
                error: function(model, error) {
                    var message = JSON.parse(error.responseText).message;
                    App.Notify('error', message, '', {
                        addclass: 'stack-bottomleft custom',
                        stack: "stack_bottomleft",
                        icon: 'picon picon-32 picon-fill-color',
                        opacity: .8,
                        nonblock: {
                            nonblock: true
                        }
                    });
                }
            });
        },
        reset: function() {

            $(".list-holder", this.$el).empty();

            this.collection.filters.reset();
        },
        fetch: function(filters) {
            this.collection.reset();
            this.collection.filters.set(filters, {
                silent: true
            });

            this.collection.fetch();
        },
        editUser: function(e) {

            var el = $(e.target).closest(".row"),
                    userId = el.attr("id"),
                    user = this.collection.get(userId),
                    $this = this;

            if (!user)
                return;

            $(".list-wrapper", this.$el).hide();
            $(".form-wrapper", this.$el).show();

            this.formView && this.formView.dispose();
            user.set("status", 1);
            this.formView = new UserFomrView({
                model: user,
                onSaveSuccess: function(obj) {
                    user.set(obj);
                    $(".form-wrapper", $this.$el).hide();
                    $(".list-wrapper", $this.$el).show();
                    $this.addRow(user, 'update');
                }
            });

            $(".form-wrapper", this.$el).html(this.formView.$el);
            this.formView.afterRender && this.formView.afterRender();
        },
        resetPassword: function(e) {

            var el = $(e.target).closest(".row"),
                    userId = el.attr("id"),
                    user = this.collection.get(userId);

            if (user) {
                user.save({
                    "password": "123456"
                }, {
                    patch: true
                });
            }
        }


    });
    return RegionsListView
});
define([
    'jquery'
], function ($) {
    var YoutubeAPI = function(){
        this.start = 1;
        this.limit = 10;
    };
    YoutubeAPI.prototype = {
    
        searchYoutube: function(config){            
           $.ajax({
                type: "GET",
                url: 'https://gdata.youtube.com/feeds/api/videos',
                data: {
                    'v'             : 2,
                    'alt'           : 'jsonc',
                    'max-result'    : this.limit,
                    'start-index'   : this.start,
                    'q'             : 'vim'
                },
                dataType:"jsonp",
                context: this,
                success: function(response){
                    console.log(response.data['items']);
                    this.updatePaging();
                    config.callBackFn(response.data['items']);
                }                
            }, this); 
        
        },
        
        updatePaging: function(){            
            this.start += this.limit;
            console.log('this.start', this.start)
        },
        
        reset: function(){
            this.start = 0;
            console.log('START', this.start)
        }
    }     

    return new YoutubeAPI();
})    
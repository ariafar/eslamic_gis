define([
    'jquery',
    'jquery-ui',
    'bootstrap',
    'underscore',
    'backbone',
    'frontend/models/user',
    'pnotify',
    ] , function ($, JU, bootstrap, _, Backbone, UserModel, PNotify) {

        var ajaxSetup = function(){
            $.ajaxSetup({
                beforeSend: function(jqXHR, settings){
                    $('.loading').modal()
                    $(".loading-image").show();
                },
                complete : function(){
                    setTimeout(function(){
                        $('.loading').modal('hide');
                        $(".loading-image").hide();
                    }, 1000)

                }
            });

        };

        var processScroll = function(){
            var $win = $(window)
            , $nav = $('#primary-nav')
            , navTop = $('#primary-nav').length && $('#primary-nav').offset().top - 74
            , isFixed = 1
            , isAddH = false;

            processScroll2();
            $win.unbind('scroll',processScroll2);
            $win.bind('scroll', processScroll2);

            function processScroll2() {
                var i, scrollTop = $win.scrollTop();

                if (scrollTop >= navTop && !isFixed) {
                    if(!isAddH){
                        //                        var h = $('#main-container').height();
                        //                        $('#main-container').height(h + 5);
                        isAddH = true;
                    }else if(isAddH){
                        //                        var h = $('#main-container').height();
                        //                        $('#main-container').height(h - 5);
                        isAddH = false;
                    }
                    isFixed = 1;
                    $('#primary-nav').addClass('subnav-fixed');
                }
                else if (scrollTop <= navTop && isFixed) {
                    isFixed = 0;
                    $('#main-container').css('height', 'none');
                    $('#primary-nav').removeClass('subnav-fixed');
                }
            }
        };


        var notify = function(type, title, message, options) {
            var stackOpt = {
                "stack_topleft" : {
                    "dir1": "down",
                    "dir2": "right",
                    "push": "top"
                },
                "stack_bottomleft" : {
                    "dir1": "right",
                    "dir2": "up",
                    "push": "top"
                },
                "stack_bar_top" : {
                    "dir1": "down",
                    "dir2": "right",
                    "push": "top",
                    "spacing1": 0,
                    "spacing2": 0
                },
                "stack_bar_bottom" : {
                    "dir1": "up",
                    "dir2": "right",
                    "spacing1": 0,
                    "spacing2": 0
                },
                "stack_context" : {
                    "dir1": "down",
                    "dir2": "left",
                    "context": $("#stack-context")
                    }
            }

            var defaults = {
                type        : "notice",
                delay       : 3000,
                styling     : 'bootstrap3',
                icon        : false,
                closer      : true,
                buttons     : {
                    closer  : true,
                    sticker : false
                },
                labels: {
                    close: "Close",
                    stick: "Stick"
                }
            };

            var options = options || {};
            var options = _.extend(defaults, options);
            
            options.title = title ? title : '';
            options.text  = message ?  message : '';
            options.type  = type ? type : 'text';
            
            options.stack  = options.stack ? stackOpt[options.stack] : stackOpt["stack-bottomright"]

            new PNotify(options);
        }

        showError = function(message){
            this.Notify('error', message, '', {
                addclass: 'stack-bottomleft custom',
                stack: "stack_bottomleft",
                icon: 'picon picon-32 picon-fill-color',
                opacity: .8,
                nonblock: {
                    nonblock: true
                }
            });
        }

        Lang = {

            code: "fa",

            defaultCode: "fa",

            tokenRegEx:  /\$\{([\w.]+?)\}/g,

            format: function(template, context, args) {
                if(!context) {
                    context = window;
                }

                var replacer = function(str, match) {
                    var replacement;
                    var subs = match.split(/\.+/);
                    for (var i=0; i< subs.length; i++) {
                        if (i == 0) {
                            replacement = context;
                        }

                        replacement = replacement[subs[i]];
                    }

                    if(typeof replacement == "function") {
                        replacement = args ?
                        replacement.apply(null, args) :
                        replacement();
                    }

                    if (typeof replacement == 'undefined') {
                        return 'undefined';
                    } else {
                        return replacement;
                    }
                };

                return template.replace(this.Lang.tokenRegEx, replacer);
            },

            translate: function(key, context) {
                if(!key) return;

                var dictionary = (GLOBAL && GLOBAL.i18n) ? GLOBAL.i18n.phrases : {},
                message;
                if(dictionary[key]){
                    message = dictionary[key];
                }else if(dictionary[key.toLowerCase()]){
                    message = dictionary[key.toLowerCase()];
                }else {
                    var naw_key = key.replace(/ /g, '').replace(/'/g,"\\" );
                    message = dictionary[naw_key.toLowerCase()];
                }

                if(!message) {
                    // Message not found, fall back to message key
                    message = key;
                }
                else{
                    message = message.replace(/\\n/g, '*+*');
                    message = message.replace(/\\/g, '\'');
                    message =  message.replace(/\*\+\*/g, '\n');
                }
                if(context) {
                    message = Application.Lang.format(message, context);
                }

                return /*'<span style="color:red">'+*/message /*+ "</span>"*/;
            }

        };

        var initialize = function() {

            window.currentUser = new UserModel();
            if(window.current_user){
                window.currentUser.set(window.current_user)
                window.currentUser.unset('password');
            }

            window.scrollTo(0, 0);

            $(window).bind('hashchange', function(){
                window.scrollTo(0, 0);
            });

        };

        return {
            initialize          : initialize,
            processScroll       : processScroll,
            ajaxSetup           : ajaxSetup,
            Lang                : Lang,
            Notify              : notify,
            showError           : showError
        };
    });

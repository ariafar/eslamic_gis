<?php

class Feeds_Service_ActivityCommentLikeTest extends Tea_Test_ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('users');
        $this->cleanupTable('activities');
        $this->cleanupTable('activity_comments');
        $this->cleanupTable('activity_comment_likes');
    }

    public function tearDown()
    {
    }

    public function testGetInstance()
    {
        $instance = Feeds_Service_ActivityCommentLike::getInstance();
        $this->assertInstanceOf('Feeds_Service_ActivityCommentLike', $instance);
    }

    public function testCreateActivityCommentLike()
    {
        /*
         * activity is created
         * a comment is created for this activity
         * 
         * activityCommentLike retrieves by comment_id, 
         * and result of this search should has no item
         * 
         * user, likes this comment
         * 
         * activityCommentLike retrieves by comment_id, 
         * and result of this search should has one item
         */
        
        $activityCommentLikeService = Feeds_Service_ActivityCommentLike::getInstance();
        
        $user                = $this->createTestUser();
        $activity            = $this->createTestActivity($user);
        $activityComment     = $this->createTestActivityComment($user, $activity);
        
        $filter = array(
            'comment_id' => $activityComment->getId()
        );
        $activityCommentLikeService->getList($filter, null, 0, $count);
        $this->assertEquals($count, 0);
        
        $activityCommentLike = new Feeds_Model_ActivityCommentLike();
        $activityCommentLike->setCommentId($activityComment->getId());
        $activityCommentLike->setLikerId($user->getId());
        $activityCommentLike->setActivityId($activity->getId());
        $activityCommentLike = $activityCommentLikeService->save($activityCommentLike);
        
        $filter = array(
            'comment_id' => $activityComment->getId()
        );
        $activityCommentLikeService->getList($filter, null, 0, $count);
        $this->assertEquals($count, 1);
    }    

    public function testDeleteActivityCommentLike()
    {
        /*
         * activity is created
         * a comment is created for this activity
         * user, likes this comment
         * 
         * activityCommentLike retrieves by comment_id, 
         * and result of this search should has one item
         * 
         * user. unlikes this comment
         * 
         * activityCommentLike retrieves by comment_id, 
         * and result of this search should has no item
         */

        $activityCommentLikeService = Feeds_Service_ActivityCommentLike::getInstance();
        
        $user                   = $this->createTestUser();
        $activity               = $this->createTestActivity($user);
        $activityComment        = $this->createTestActivityComment($user, $activity);
        $activityCommentLike    = $this->createTestActivityCommentLike($user, $activity, $activityComment);
        
        $filter = array(
            'comment_id' => $activityComment->getId()
        );
        $activityCommentLikeService->getList($filter, null, 0, $count);
        $this->assertEquals($count, 1);
        
        $activityCommentLikeService->remove($activityCommentLike);
        
        $filter = array(
            'comment_id' => $activityComment->getId()
        );
        $activityCommentLikeService->getList($filter, null, 0, $count);
        $this->assertEquals($count, 0);
    }
}

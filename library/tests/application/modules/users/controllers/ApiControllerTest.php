<?php

class Users_ApiControllerTest extends Tea_Test_ControllerTestCase
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testIndexAction()
    {
        $this->cleanupTable('users');

        $this->dispatch('/api/users');
        $this->assertResponseCode(200);
        $this->assertModule('users');
        $this->assertController('Api');
        $this->assertAction('index');
    }
    
    public function testPostAction()
    {
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'first_name' => 'first_name',
                    'last_name'  => 'last_name',
                    'email'      => TEST_USER_EMAIL,
                    'password'   => TEST_USER_PASS,
                    'gender'     => 2,
                    'timezone'   => 'timezone',
                    'locale'     => 'locale'
                )
            );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('api/users');
        $this->assertResponseCode(200);
        $result = $this->response->getBody();
        $user   = json_decode($result, true);

        $this->assertEquals($user['first_name'], 'first_name');
        $this->assertEquals($user['last_name'], 'last_name');
        $this->assertEquals($user['email'], TEST_USER_EMAIL);
        $this->assertEquals($user['password'], TEST_USER_PASS);
        $this->assertEquals($user['gender'], 2);
        $this->assertEquals($user['timezone'], 'timezone');
        $this->assertEquals($user['locale'], 'locale');

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('POST')
            ->setPost(
                array(
                    'email'    => TEST_USER_EMAIL,
                    'password' => TEST_USER_PASS
                )
            );
        $this->request->setHeader('Accept', 'application/json');
        $this->dispatch('/users/auth/login');
        $this->assertResponseCode(200);

        $this->resetRequest();
        $this->resetResponse();
        $this->dispatch('/api/users');
        $this->assertResponseCode(200);
        $this->assertModule('users');
        $this->assertController('Api');
        $this->assertAction('index');
        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($this->response->getBody(), true);
        $this->assertEquals($result['count'], 1);

        return $user['id'];
    }

    /**
     * @depends testPostAction
     */
    public function testGetAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('GET');
        $this->dispatch('/api/users/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('users');
        $this->assertController('Api');
        $this->assertAction('get');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $result = json_decode($body, true);
        $this->assertEquals($result['id'], $id);

        return $id;
    }

    /**
     * @depends testGetAction
     */
    public function testPutAction($id)
    {
        return $id;
    }

    /**
     * @depends testPutAction
     */
    public function testDeleteAction($id)
    {
        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('DELETE');
        $this->dispatch('/api/users/' . $id);
        $this->assertResponseCode(200);
        $this->assertModule('users');
        $this->assertController('Api');
        $this->assertAction('delete');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'Success');

        $this->resetRequest();
        $this->resetResponse();
        $this->request->setMethod('GET');
        $this->dispatch('/api/users/' . $id);
        $this->assertResponseCode(404);
        $this->assertModule('users');
        $this->assertController('Api');
        $this->assertAction('get');

        $body = $this->response->getBody();
        $this->assertNotEmpty($body);
        $this->assertEquals($body, 'Not Found User');
    }
}


<?php

class AssigneeTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('follows');
        $this->cleanupTable('shares');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('poll_votes');
        $this->cleanupTable('poll_voting_items');
        $this->cleanupTable('poll_assignees');
        $this->cleanupTable('poll_voting_responses');
        $this->cleanupTable('polls');
        $this->cleanupTable('users');
    }

    public function testGetInstance()
    {
        $instance = Polls_Service_Assignee::getInstance();
        $this->assertInstanceOf('Polls_Service_Assignee', $instance);
    }

    public function testCreateAssignee()
    {
        $assigneeService = Polls_Service_Assignee::getInstance();
        $assignee        = new Polls_Model_Assignee();
        $user            = $this->createTestUser();
        $assignee->setStatus(2);
        $assignee->setNew(true);
        $assignee->setAssigneeId($user->getId());
        $assignee->setCreatedById($user->getId());
        $assignee->setUpdatedById($user->getId());

        $pollService = Polls_Service_Poll::getInstance();
        $poll        = new Polls_Model_Poll();
        $poll->setTitle('test title');
        $poll->setDescription('test description');
        $poll->setStatus(2);
        $poll->setPrivacy(1);
        $poll->setNew(true);
        $poll->setCreatedById($user->getId());
        $poll->setUpdatedById($user->getId());
        $poll        = $pollService->save($poll);

        $assignee->setPollId($poll->getId());
        $assignee = $assigneeService->save($assignee);
        $testpoll = $assigneeService->getByPK($assignee->getAssigneeId(), $assignee->getPollId());
        $this->assertEquals($assignee, $testpoll);
    }

    public function testUpdatePoll()
    {
        $assigneeService = Polls_Service_Assignee::getInstance();
        $assignee        = new Polls_Model_Assignee();
        $user            = $this->createTestUser();
        $assignee->setStatus(2);
        $assignee->setNew(true);
        $assignee->setAssigneeId($user->getId());
        $assignee->setCreatedById($user->getId());
        $assignee->setUpdatedById($user->getId());

        $pollService = Polls_Service_Poll::getInstance();
        $poll        = new Polls_Model_Poll();
        $poll->setTitle('test title');
        $poll->setDescription('test description');
        $poll->setStatus(2);
        $poll->setPrivacy(1);
        $poll->setNew(true);
        $poll->setCreatedById($user->getId());
        $poll->setUpdatedById($user->getId());
        $poll        = $pollService->save($poll);

        $assignee->setPollId($poll->getId());
        $assignee = $assigneeService->save($assignee);
        $assignee->setNew(FALSE);
        $assignee->setStatus(1);
        $assignee = $assigneeService->save($assignee);
        $this->assertEquals($assignee->getStatus(), 1);
    }

    public function testRemovePoll()
    {
        $assigneeService = Polls_Service_Assignee::getInstance();
        $assignee        = new Polls_Model_Assignee();
        $user            = $this->createTestUser();
        $assignee->setStatus(2);
        $assignee->setNew(true);
        $assignee->setAssigneeId($user->getId());
        $assignee->setCreatedById($user->getId());
        $assignee->setUpdatedById($user->getId());

        $pollService = Polls_Service_Poll::getInstance();
        $poll        = new Polls_Model_Poll();
        $poll->setTitle('test title');
        $poll->setDescription('test description');
        $poll->setStatus(2);
        $poll->setPrivacy(1);
        $poll->setNew(true);
        $poll->setCreatedById($user->getId());
        $poll->setUpdatedById($user->getId());
        $poll        = $pollService->save($poll);

        $assignee->setPollId($poll->getId());
        $assignee   = $assigneeService->save($assignee);
        $assigneeId = $assignee->getId();
        $assigneeService->remove($assignee);
        $assignee   = $assigneeService->getByPK($assigneeId, $poll->getId());
        $this->assertEquals($assignee, null);
    }

}

?>

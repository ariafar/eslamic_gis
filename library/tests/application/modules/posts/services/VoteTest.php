<?php

class Posts_Service_VoteTest extends Tea_Test_ControllerTestCase
{

    public function setUp()
    {
        parent::setUp();
        $this->cleanupTable('likes');
        $this->cleanupTable('comments');
        $this->cleanupTable('follows');
        $this->cleanupTable('shares');
        $this->cleanupTable('votes');
        $this->cleanupTable('posts');
        $this->cleanupTable('users');
    }

    public function testGetInstance()
    {
        $instance = Posts_Service_Vote::getInstance();
        $this->assertInstanceOf('Posts_Service_Vote', $instance);
    }

    public function testCreateVote()
    {
        $voteService = Posts_Service_Vote::getInstance();
        $vote        = new Posts_Model_Vote();
        $user        = $this->createTestUser();
        $post        = $this->createTestPost($user);
        $vote->setPostId($post->getId());
        $vote->setVoterId($user->getId());
        $vote->setType('1');
        $vote->setNew(true);
        $vote        = $voteService->save($vote);
        $testVote    = $voteService->getByPK($vote->getPostId(), $vote->getVoterId());
        $this->assertEquals($vote, $testVote);
        $this->assertEquals($testVote->getPostId(), $post->getId());
        $this->assertEquals($testVote->getVoterId(), $user->getId());
        $this->assertEquals($testVote->getType(), '1');
    }

}

?>

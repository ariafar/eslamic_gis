<?php

class Tea_Controller_Plugin_Accept extends Zend_Controller_Plugin_Abstract
{
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
		parent::dispatchLoopStartup($request);
        if ($request->isXmlHttpRequest() || $request->getParam('ajax', false) == 'true') {
            $request->setParam('format', 'ajax');
        }

        $header = $request->getHeader('Accept');
        switch (true) {
        case (strstr($header, 'application/json')):
            $request->setParam('format', 'json');
            break;
        }

        if ($request->getModuleName() == 'api') {
            $request->setParam('format', 'json');
        }
    }
}

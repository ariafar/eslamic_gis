<?php

class Tea_Controller_Plugin_SetTimezone extends Zend_Controller_Plugin_Abstract
{
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        $auth = Zend_Auth::getInstance();
        $tz   = null;

        if ($auth->hasIdentity()) {
            $user = $auth->getIdentity();
            $tz   = $user->getTimezone();
        }

        if ($tz == null && extension_loaded('geoip')) {
            if (geoip_db_avail(GEOIP_REGION_EDITION_REV0)) {
                $userIP  = $this->_getUserIp();
                $region  = geoip_region_by_name($userIP);
                $tz      = @geoip_time_zone_by_country_and_region(
                    $region['country_code'],
                    $region['region']
                );
            } elseif (geoip_db_avail(GEOIP_CITY_EDITION_REV0)) {
                $userIP = $this->_getUserIp();
                $info   = @geoip_record_by_name($userIP);
                if (is_array($info)) {
                    $tz = @geoip_time_zone_by_country_and_region(
                        $info['country_code'],
                        $info['region']
                    );
                }
            }
        }

        if ($tz != null) {
            date_default_timezone_set($tz);
        }
    }

    private function _getUserIp()
    {
        $keys = array(
            'HTTP_CLIENT_IP',
            'HTTP_X_FORWARDED_FOR',
            'HTTP_X_FORWARDED',
            'HTTP_X_CLUSTER_CLIENT_IP',
            'HTTP_FORWARDED_FOR',
            'HTTP_FORWARDED',
            'REMOTE_ADDR'
        );

        foreach ($keys as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP) !== false) {
                        return $ip;
                    }
                }
            }
        }

        return null;
    }
}

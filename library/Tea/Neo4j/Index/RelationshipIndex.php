<?php
//namespace Everyman\Neo4j\Index;
//
//use Everyman\Neo4j\Client,
//    Everyman\Neo4j\Index;

/**
 * Represents a relationship index in the database
 */
class Tea_Neo4j_Index_RelationshipIndex extends Tea_Neo4j_Index
{
	/**
	 * Initialize the index
	 *
	 * @param Client $client
	 * @param string $name
	 * @param array  $config
	 */
	public function __construct(Tea_Neo4j_Client $client, $name, $config=array())
	{
		parent::__construct($client, Tea_Neo4j_Index::TypeRelationship, $name, $config);
	}
}

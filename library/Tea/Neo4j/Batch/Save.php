<?php
//namespace Everyman\Neo4j\Batch;
//
//use Everyman\Neo4j\Batch,
//	Everyman\Neo4j\Command\Batch as Command,
//	Everyman\Neo4j\Node,
//	Everyman\Neo4j\Relationship,
//	Everyman\Neo4j\PropertyContainer;

/**
 * A save operation
 */
class Tea_Neo4j_Batch_Save extends Tea_Neo4j_Batch_Operation
{
	protected $command = null;

	/**
	 * Build the operation
	 *
	 * @param Tea_Neo4j_Batch $batch
	 * @param Tea_Neo4j_PropertyContainer $entity
	 * @param integer $opId
	 */
	public function __construct(Tea_Neo4j_Batch $batch, Tea_Neo4j_PropertyContainer $entity, $opId)
	{
		parent::__construct($batch, 'save', $entity, $opId);
	}

	/**
	 * Get the command that represents this operation
	 *
	 * @return Batch\Command
	 */
	public function getCommand()
	{
		if (!$this->command) {
			$entity = $this->entity;
			$command = null;
			if (!$entity->hasId()) {
				if ($entity instanceof Tea_Neo4j_Node) {
					$command = new Tea_Neo4j_Command_Batch_CreateNode($this->batch->getClient(), $entity, $this->opId);
				} else if ($entity instanceof Tea_Neo4j_Relationship) {
					$command = new Tea_Neo4j_Command_Batch_CreateRelationship($this->batch->getClient(), $entity, $this->opId, $this->batch);
				}
			} else {
				if ($entity instanceof Tea_Neo4j_Node) {
					$command = new Tea_Neo4j_Command_Batch_UpdateNode($this->batch->getClient(), $entity, $this->opId);
				} else if ($entity instanceof Tea_Neo4j_Relationship) {
					$command = new Tea_Neo4j_Command_Batch_UpdateRelationship($this->batch->getClient(), $entity, $this->opId);
				}
			}

			$this->command = $command;
		}
		return $this->command;
	}
}

<?php
//namespace Everyman\Neo4j\Batch;
//
//use Everyman\Neo4j\Batch,
//	Everyman\Neo4j\Command\Batch as Command,
//	Everyman\Neo4j\Node,
//	Everyman\Neo4j\Relationship,
//	Everyman\Neo4j\PropertyContainer;

/**
 * A delete operation
 */
class Tea_Neo4j_Batch_Delete extends Tea_Neo4j_Batch_Operation
{
	protected $command = null;

	/**
	 * Build the operation
	 *
	 * @param Tea_Neo4j_Batch $batch
	 * @param Tea_Neo4j_PropertyContainer $entity
	 * @param integer $opId
	 */
	public function __construct(Tea_Neo4j_Batch $batch, Tea_Neo4j_PropertyContainer $entity, $opId)
	{
		parent::__construct($batch, 'delete', $entity, $opId);
	}

	/**
	 * Get the command that represents this operation
	 *
	 * @return Batch\Command
	 */
	public function getCommand()
	{
		if (!$this->command) {
			$entity = $this->entity;
			$command = null;
			if ($entity instanceof Tea_Neo4j_Node) {
				$command = new Tea_Neo4j_Command_Batch_DeleteNode($this->batch->getClient(), $entity, $this->opId);
			} else if ($entity instanceof Tea_Neo4j_Relationship) {
				$command = new Tea_Neo4j_Command_Batch_DeleteRelationship($this->batch->getClient(), $entity, $this->opId);
			}

			$this->command = $command;
		}
		return $this->command;
	}
}

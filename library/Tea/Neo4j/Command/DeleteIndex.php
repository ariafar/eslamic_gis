<?php
//namespace Everyman\Neo4j\Command;
//use Everyman\Neo4j\Command,
//	Everyman\Neo4j\Client,
//	Everyman\Neo4j\Exception,
//	Everyman\Neo4j\Index;

/**
 * Create an index
 */
class Tea_Neo4j_Command_DeleteIndex extends Tea_Neo4j_Command
{
	protected $index = null;

	/**
	 * Set the index to drive the command
	 *
	 * @param Tea_Neo4j_Client $client
	 * @param Tea_Neo4j_Index $index
	 */
	public function __construct(Tea_Neo4j_Client $client, Tea_Neo4j_Index $index)
	{
		parent::__construct($client);
		$this->index = $index;
	}

	/**
	 * Return the data to pass
	 *
	 * @return mixed
	 */
	protected function getData()
	{
		return null;
	}

	/**
	 * Return the transport method to call
	 *
	 * @return string
	 */
	protected function getMethod()
	{
		return 'delete';
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		$type = trim((string)$this->index->getType());
		if ($type != Tea_Neo4j_Index::TypeNode && $type != Tea_Neo4j_Index::TypeRelationship) {
			throw new Tea_Neo4j_Exception('No type specified for index');
		}

		$name = trim((string)$this->index->getName());
		if (!$name) {
			throw new Tea_Neo4j_Exception('No name specified for index');
		}
		$name = rawurlencode($name);

		return '/index/'.$type.'/'.$name;
	}

	/**
	 * Use the results
	 *
	 * @param integer $code
	 * @param array   $headers
	 * @param array   $data
	 * @return integer on failure
	 */
	protected function handleResult($code, $headers, $data)
	{
		if ((int)($code / 100) != 2) {
			$this->throwException('Unable to delete index', $code, $headers, $data);
		}
		return true;
	}
}


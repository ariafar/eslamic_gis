<?php
//namespace Everyman\Neo4j\Command;
//use Everyman\Neo4j\Command,
//	Everyman\Neo4j\Client,
//	Everyman\Neo4j\Exception,
//	Everyman\Neo4j\Relationship,
//	Everyman\Neo4j\Node,
//	Everyman\Neo4j\Index;

/**
 * Queries for entities in an index
 */
class Tea_Neo4j_Command_QueryIndex extends Tea_Neo4j_Command_SearchIndex 
{
	/**
	 * Set the index to drive the command
	 *
	 * @param Tea_Neo4j_Client $client
	 * @param Tea_Neo4j_Index $index
	 * @param string $query
	 */
	public function __construct(Tea_Neo4j_Client $client, Tea_Neo4j_Index $index, $query)
	{
		parent::__construct($client, $index, $query, null);
	}

	/**
	 * Return the path to use
	 *
	 * @return string
	 */
	protected function getPath()
	{
		$path = parent::getPath();
		$path = join('/', array_slice(explode('/', $path), 0,4));
		$query = rawurlencode($this->key);
		return $path . '?query=' . $query;
	}
}


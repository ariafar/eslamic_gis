<?php
//namespace Everyman\Neo4j\Command\Batch;
//use Everyman\Neo4j\Client,
//	Everyman\Neo4j\Node,
//	Everyman\Neo4j\Command\DeleteNode as SingleDeleteNode;

/**
 * Delete a node in a batch
 */
class Tea_Neo4j_Command_Batch_DeleteNode extends Tea_Neo4j_Command_Batch_Command
{
	/**
	 * Set the operation to drive the command
	 *
	 * @param Tea_Neo4j_Client $client
	 * @param Tea_Neo4j_Node $node
	 * @param integer $opId
	 */
	public function __construct(Tea_Neo4j_Client $client, Tea_Neo4j_Node $node, $opId)
	{
		parent::__construct($client, new Tea_Neo4j_Command_DeleteNode($client, $node), $opId);
	}

	/**
	 * Return the data to pass
	 *
	 * @return array
	 */
	protected function getData()
	{
		$opData = array(array(
			'method' => strtoupper($this->base->getMethod()),
			'to' => $this->base->getPath(),
			'id' => $this->opId,
		));
		return $opData;
	}
}

